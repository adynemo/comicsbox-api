<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210918031049 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Fix Issue and define number as nullable';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX idx_12ad233e3da5256d');
        $this->addSql('ALTER TABLE issue ALTER number DROP NOT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_12AD233E3DA5256D ON issue (image_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX UNIQ_12AD233E3DA5256D');
        $this->addSql('ALTER TABLE issue ALTER number SET NOT NULL');
        $this->addSql('CREATE INDEX idx_12ad233e3da5256d ON issue (image_id)');
    }
}
