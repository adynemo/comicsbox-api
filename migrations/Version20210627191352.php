<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210627191352 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create Character and add endYear and URL to Publisher';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE character (id UUID NOT NULL, first_appearance_id UUID DEFAULT NULL, avatar_id UUID DEFAULT NULL, name VARCHAR(255) NOT NULL, presentation TEXT DEFAULT NULL, creation_year INT DEFAULT NULL, created_at DATE NOT NULL, updated_at DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_937AB03447F23211 ON character (first_appearance_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_937AB03486383B10 ON character (avatar_id)');
        $this->addSql('COMMENT ON COLUMN character.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN character.first_appearance_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN character.avatar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE character_issue (character_id UUID NOT NULL, issue_id UUID NOT NULL, PRIMARY KEY(character_id, issue_id))');
        $this->addSql('CREATE INDEX IDX_EC6EC45C1136BE75 ON character_issue (character_id)');
        $this->addSql('CREATE INDEX IDX_EC6EC45C5E7AA58C ON character_issue (issue_id)');
        $this->addSql('COMMENT ON COLUMN character_issue.character_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN character_issue.issue_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE character_avatar (id UUID NOT NULL, file_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN character_avatar.id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE character ADD CONSTRAINT FK_937AB03447F23211 FOREIGN KEY (first_appearance_id) REFERENCES issue (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE character ADD CONSTRAINT FK_937AB03486383B10 FOREIGN KEY (avatar_id) REFERENCES character_avatar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE character_issue ADD CONSTRAINT FK_EC6EC45C1136BE75 FOREIGN KEY (character_id) REFERENCES character (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE character_issue ADD CONSTRAINT FK_EC6EC45C5E7AA58C FOREIGN KEY (issue_id) REFERENCES issue (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE publisher ADD end_year INT DEFAULT NULL');
        $this->addSql('ALTER TABLE publisher ADD url VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE character_issue DROP CONSTRAINT FK_EC6EC45C1136BE75');
        $this->addSql('ALTER TABLE character DROP CONSTRAINT FK_937AB03486383B10');
        $this->addSql('DROP TABLE character');
        $this->addSql('DROP TABLE character_issue');
        $this->addSql('DROP TABLE character_avatar');
        $this->addSql('ALTER TABLE publisher DROP end_year');
        $this->addSql('ALTER TABLE publisher DROP url');
    }
}
