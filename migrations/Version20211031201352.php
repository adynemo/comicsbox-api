<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211031201352 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Rename issue to book and create issue/TPB relation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE issue_author_relation_author_role DROP CONSTRAINT fk_399ea6653f0ccaca');
        $this->addSql('ALTER TABLE issue_author_relation DROP CONSTRAINT fk_9556994f5e7aa58c');
        $this->addSql('ALTER TABLE "character" DROP CONSTRAINT fk_937ab03447f23211');
        $this->addSql('ALTER TABLE character_issue DROP CONSTRAINT fk_ec6ec45c5e7aa58c');
        $this->addSql('ALTER TABLE issue_read DROP CONSTRAINT fk_c0b089ab5e7aa58c');
        $this->addSql('CREATE TABLE book (id UUID NOT NULL, image_id UUID DEFAULT NULL, country_id UUID DEFAULT NULL, format_id UUID DEFAULT NULL, serie_id UUID NOT NULL, title VARCHAR(255) NOT NULL, description TEXT DEFAULT \'\' NOT NULL, number INT DEFAULT NULL, page_count INT DEFAULT NULL, release_date DATE NOT NULL, isbn VARCHAR(13) DEFAULT NULL, sku VARCHAR(255) DEFAULT NULL, price DOUBLE PRECISION DEFAULT NULL, created_at DATE NOT NULL, updated_at DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CBE5A3313DA5256D ON book (image_id)');
        $this->addSql('CREATE INDEX IDX_CBE5A331F92F3E70 ON book (country_id)');
        $this->addSql('CREATE INDEX IDX_CBE5A331D629F605 ON book (format_id)');
        $this->addSql('CREATE INDEX IDX_CBE5A331D94388BD ON book (serie_id)');
        $this->addSql('CREATE TABLE issue_tpb (book_source UUID NOT NULL, book_target UUID NOT NULL, PRIMARY KEY(book_source, book_target))');
        $this->addSql('CREATE INDEX IDX_AFBEBD21765D74FE ON issue_tpb (book_source)');
        $this->addSql('CREATE INDEX IDX_AFBEBD216FB82471 ON issue_tpb (book_target)');
        $this->addSql('CREATE TABLE book_author_relation (id UUID NOT NULL, book_id UUID NOT NULL, author_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9AEFF8716A2B381 ON book_author_relation (book_id)');
        $this->addSql('CREATE INDEX IDX_9AEFF87F675F31B ON book_author_relation (author_id)');
        $this->addSql('CREATE TABLE book_author_relation_author_role (book_author_relation_id UUID NOT NULL, author_role_id UUID NOT NULL, PRIMARY KEY(book_author_relation_id, author_role_id))');
        $this->addSql('CREATE INDEX IDX_F86D29B72071BBA1 ON book_author_relation_author_role (book_author_relation_id)');
        $this->addSql('CREATE INDEX IDX_F86D29B79339BDEF ON book_author_relation_author_role (author_role_id)');
        $this->addSql('CREATE TABLE book_read (id UUID NOT NULL, reader_id UUID NOT NULL, book_id UUID NOT NULL, count INT DEFAULT 1 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_81CA0A6F1717D737 ON book_read (reader_id)');
        $this->addSql('CREATE INDEX IDX_81CA0A6F16A2B381 ON book_read (book_id)');
        $this->addSql('CREATE TABLE character_book (character_id UUID NOT NULL, book_id UUID NOT NULL, PRIMARY KEY(character_id, book_id))');
        $this->addSql('CREATE INDEX IDX_DC19B7A91136BE75 ON character_book (character_id)');
        $this->addSql('CREATE INDEX IDX_DC19B7A916A2B381 ON character_book (book_id)');
        $this->addSql('ALTER TABLE book ADD CONSTRAINT FK_CBE5A3313DA5256D FOREIGN KEY (image_id) REFERENCES cover (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book ADD CONSTRAINT FK_CBE5A331F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book ADD CONSTRAINT FK_CBE5A331D629F605 FOREIGN KEY (format_id) REFERENCES format (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book ADD CONSTRAINT FK_CBE5A331D94388BD FOREIGN KEY (serie_id) REFERENCES serie (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_tpb ADD CONSTRAINT FK_AFBEBD21765D74FE FOREIGN KEY (book_source) REFERENCES book (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_tpb ADD CONSTRAINT FK_AFBEBD216FB82471 FOREIGN KEY (book_target) REFERENCES book (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_author_relation ADD CONSTRAINT FK_9AEFF8716A2B381 FOREIGN KEY (book_id) REFERENCES book (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_author_relation ADD CONSTRAINT FK_9AEFF87F675F31B FOREIGN KEY (author_id) REFERENCES author (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_author_relation_author_role ADD CONSTRAINT FK_F86D29B72071BBA1 FOREIGN KEY (book_author_relation_id) REFERENCES book_author_relation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_author_relation_author_role ADD CONSTRAINT FK_F86D29B79339BDEF FOREIGN KEY (author_role_id) REFERENCES author_role (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_read ADD CONSTRAINT FK_81CA0A6F1717D737 FOREIGN KEY (reader_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book_read ADD CONSTRAINT FK_81CA0A6F16A2B381 FOREIGN KEY (book_id) REFERENCES book (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE character_book ADD CONSTRAINT FK_DC19B7A91136BE75 FOREIGN KEY (character_id) REFERENCES character (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE character_book ADD CONSTRAINT FK_DC19B7A916A2B381 FOREIGN KEY (book_id) REFERENCES book (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE issue_author_relation');
        $this->addSql('DROP TABLE issue_author_relation_author_role');
        $this->addSql('DROP TABLE issue');
        $this->addSql('DROP TABLE character_issue');
        $this->addSql('DROP TABLE issue_read');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE issue_tpb DROP CONSTRAINT FK_AFBEBD21765D74FE');
        $this->addSql('ALTER TABLE issue_tpb DROP CONSTRAINT FK_AFBEBD216FB82471');
        $this->addSql('ALTER TABLE book_author_relation DROP CONSTRAINT FK_9AEFF8716A2B381');
        $this->addSql('ALTER TABLE book_read DROP CONSTRAINT FK_81CA0A6F16A2B381');
        $this->addSql('ALTER TABLE character DROP CONSTRAINT FK_937AB03447F23211');
        $this->addSql('ALTER TABLE character_book DROP CONSTRAINT FK_DC19B7A916A2B381');
        $this->addSql('ALTER TABLE book_author_relation_author_role DROP CONSTRAINT FK_F86D29B72071BBA1');
        $this->addSql('CREATE TABLE issue_author_relation (id UUID NOT NULL, issue_id UUID NOT NULL, author_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_9556994ff675f31b ON issue_author_relation (author_id)');
        $this->addSql('CREATE INDEX idx_9556994f5e7aa58c ON issue_author_relation (issue_id)');
        $this->addSql('CREATE TABLE issue_author_relation_author_role (issue_author_relation_id UUID NOT NULL, author_role_id UUID NOT NULL, PRIMARY KEY(issue_author_relation_id, author_role_id))');
        $this->addSql('CREATE INDEX idx_399ea6659339bdef ON issue_author_relation_author_role (author_role_id)');
        $this->addSql('CREATE INDEX idx_399ea6653f0ccaca ON issue_author_relation_author_role (issue_author_relation_id)');
        $this->addSql('CREATE TABLE issue (id UUID NOT NULL, image_id UUID DEFAULT NULL, serie_id UUID NOT NULL, country_id UUID DEFAULT NULL, format_id UUID DEFAULT NULL, title VARCHAR(255) NOT NULL, description TEXT DEFAULT \'\' NOT NULL, number INT DEFAULT NULL, page_count INT DEFAULT NULL, release_date DATE NOT NULL, created_at DATE NOT NULL, updated_at DATE DEFAULT NULL, isbn VARCHAR(13) DEFAULT NULL, sku VARCHAR(255) DEFAULT NULL, price DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_12ad233ed629f605 ON issue (format_id)');
        $this->addSql('CREATE INDEX idx_12ad233ef92f3e70 ON issue (country_id)');
        $this->addSql('CREATE INDEX idx_12ad233ed94388bd ON issue (serie_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_12ad233e3da5256d ON issue (image_id)');
        $this->addSql('CREATE TABLE character_issue (character_id UUID NOT NULL, issue_id UUID NOT NULL, PRIMARY KEY(character_id, issue_id))');
        $this->addSql('CREATE INDEX idx_ec6ec45c1136be75 ON character_issue (character_id)');
        $this->addSql('CREATE INDEX idx_ec6ec45c5e7aa58c ON character_issue (issue_id)');
        $this->addSql('CREATE TABLE issue_read (id UUID NOT NULL, reader_id UUID NOT NULL, issue_id UUID NOT NULL, count INT DEFAULT 1 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_c0b089ab5e7aa58c ON issue_read (issue_id)');
        $this->addSql('CREATE INDEX idx_c0b089ab1717d737 ON issue_read (reader_id)');
        $this->addSql('ALTER TABLE issue_author_relation ADD CONSTRAINT fk_9556994f5e7aa58c FOREIGN KEY (issue_id) REFERENCES issue (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_author_relation ADD CONSTRAINT fk_9556994ff675f31b FOREIGN KEY (author_id) REFERENCES author (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_author_relation_author_role ADD CONSTRAINT fk_399ea6653f0ccaca FOREIGN KEY (issue_author_relation_id) REFERENCES issue_author_relation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_author_relation_author_role ADD CONSTRAINT fk_399ea6659339bdef FOREIGN KEY (author_role_id) REFERENCES author_role (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT fk_12ad233ed94388bd FOREIGN KEY (serie_id) REFERENCES serie (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT fk_12ad233e3da5256d FOREIGN KEY (image_id) REFERENCES cover (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT fk_12ad233ef92f3e70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT fk_12ad233ed629f605 FOREIGN KEY (format_id) REFERENCES format (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE character_issue ADD CONSTRAINT fk_ec6ec45c1136be75 FOREIGN KEY (character_id) REFERENCES "character" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE character_issue ADD CONSTRAINT fk_ec6ec45c5e7aa58c FOREIGN KEY (issue_id) REFERENCES issue (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_read ADD CONSTRAINT fk_c0b089ab1717d737 FOREIGN KEY (reader_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_read ADD CONSTRAINT fk_c0b089ab5e7aa58c FOREIGN KEY (issue_id) REFERENCES issue (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE book');
        $this->addSql('DROP TABLE issue_tpb');
        $this->addSql('DROP TABLE book_author_relation');
        $this->addSql('DROP TABLE book_author_relation_author_role');
        $this->addSql('DROP TABLE book_read');
        $this->addSql('DROP TABLE character_book');
        $this->addSql('ALTER TABLE character DROP CONSTRAINT fk_937ab03447f23211');
        $this->addSql('ALTER TABLE character ALTER publisher_id TYPE UUID');
        $this->addSql('ALTER TABLE character ALTER publisher_id DROP DEFAULT');
        $this->addSql('ALTER TABLE character ALTER creators_id TYPE UUID');
        $this->addSql('ALTER TABLE character ALTER creators_id DROP DEFAULT');
        $this->addSql('ALTER TABLE character ADD CONSTRAINT fk_937ab03447f23211 FOREIGN KEY (first_appearance_id) REFERENCES issue (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE country ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE country ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE format ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE format ALTER id DROP DEFAULT');
    }
}
