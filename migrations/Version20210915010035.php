<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210915010035 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Refactor of the images relations, add it to Serie and create logo for publisher';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "character" DROP CONSTRAINT fk_937ab03486383b10');
        $this->addSql('ALTER TABLE author DROP CONSTRAINT fk_bdafd8c886383b10');
        $this->addSql('ALTER TABLE publisher DROP CONSTRAINT fk_9ce8d546f98f144a');
        $this->addSql('CREATE TABLE logo (id UUID NOT NULL, file_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN logo.id IS \'(DC2Type:uuid)\'');
        $this->addSql('DROP TABLE character_avatar');
        $this->addSql('DROP TABLE author_avatar');
        $this->addSql('DROP TABLE publisher_logo');
        $this->addSql('DROP INDEX uniq_bdafd8c886383b10');
        $this->addSql('ALTER TABLE author RENAME COLUMN avatar_id TO image_id');
        $this->addSql('ALTER TABLE author ADD CONSTRAINT FK_BDAFD8C83DA5256D FOREIGN KEY (image_id) REFERENCES avatar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BDAFD8C83DA5256D ON author (image_id)');
        $this->addSql('DROP INDEX uniq_937ab03486383b10');
        $this->addSql('ALTER TABLE "character" RENAME COLUMN avatar_id TO image_id');
        $this->addSql('ALTER TABLE "character" ADD CONSTRAINT FK_937AB0343DA5256D FOREIGN KEY (image_id) REFERENCES avatar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_937AB0343DA5256D ON "character" (image_id)');
        $this->addSql('ALTER TABLE issue DROP CONSTRAINT fk_12ad233e922726e9');
        $this->addSql('DROP INDEX idx_12ad233e922726e9');
        $this->addSql('ALTER TABLE issue RENAME COLUMN cover_id TO image_id');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233E3DA5256D FOREIGN KEY (image_id) REFERENCES cover (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_12AD233E3DA5256D ON issue (image_id)');
        $this->addSql('DROP INDEX uniq_9ce8d546f98f144a');
        $this->addSql('ALTER TABLE publisher RENAME COLUMN logo_id TO image_id');
        $this->addSql('ALTER TABLE publisher ADD CONSTRAINT FK_9CE8D5463DA5256D FOREIGN KEY (image_id) REFERENCES logo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9CE8D5463DA5256D ON publisher (image_id)');
        $this->addSql('ALTER TABLE serie ADD image_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN serie.image_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE serie ADD CONSTRAINT FK_AA3A93343DA5256D FOREIGN KEY (image_id) REFERENCES cover (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AA3A93343DA5256D ON serie (image_id)');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT fk_8d93d64986383b10');
        $this->addSql('DROP INDEX uniq_8d93d64986383b10');
        $this->addSql('ALTER TABLE "user" RENAME COLUMN avatar_id TO image_id');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT FK_8D93D6493DA5256D FOREIGN KEY (image_id) REFERENCES avatar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D6493DA5256D ON "user" (image_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE publisher DROP CONSTRAINT FK_9CE8D5463DA5256D');
        $this->addSql('CREATE TABLE character_avatar (id UUID NOT NULL, file_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN character_avatar.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE author_avatar (id UUID NOT NULL, file_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN author_avatar.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE publisher_logo (id UUID NOT NULL, file_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN publisher_logo.id IS \'(DC2Type:uuid)\'');
        $this->addSql('DROP TABLE logo');
        $this->addSql('ALTER TABLE "user" DROP CONSTRAINT FK_8D93D6493DA5256D');
        $this->addSql('DROP INDEX UNIQ_8D93D6493DA5256D');
        $this->addSql('ALTER TABLE "user" RENAME COLUMN image_id TO avatar_id');
        $this->addSql('ALTER TABLE "user" ADD CONSTRAINT fk_8d93d64986383b10 FOREIGN KEY (avatar_id) REFERENCES avatar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_8d93d64986383b10 ON "user" (avatar_id)');
        $this->addSql('ALTER TABLE character DROP CONSTRAINT FK_937AB0343DA5256D');
        $this->addSql('DROP INDEX UNIQ_937AB0343DA5256D');
        $this->addSql('ALTER TABLE character RENAME COLUMN image_id TO avatar_id');
        $this->addSql('ALTER TABLE character ADD CONSTRAINT fk_937ab03486383b10 FOREIGN KEY (avatar_id) REFERENCES character_avatar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_937ab03486383b10 ON character (avatar_id)');
        $this->addSql('ALTER TABLE issue DROP CONSTRAINT FK_12AD233E3DA5256D');
        $this->addSql('DROP INDEX IDX_12AD233E3DA5256D');
        $this->addSql('ALTER TABLE issue RENAME COLUMN image_id TO cover_id');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT fk_12ad233e922726e9 FOREIGN KEY (cover_id) REFERENCES cover (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_12ad233e922726e9 ON issue (cover_id)');
        $this->addSql('ALTER TABLE author DROP CONSTRAINT FK_BDAFD8C83DA5256D');
        $this->addSql('DROP INDEX UNIQ_BDAFD8C83DA5256D');
        $this->addSql('ALTER TABLE author RENAME COLUMN image_id TO avatar_id');
        $this->addSql('ALTER TABLE author ADD CONSTRAINT fk_bdafd8c886383b10 FOREIGN KEY (avatar_id) REFERENCES author_avatar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_bdafd8c886383b10 ON author (avatar_id)');
        $this->addSql('ALTER TABLE serie DROP CONSTRAINT FK_AA3A93343DA5256D');
        $this->addSql('DROP INDEX UNIQ_AA3A93343DA5256D');
        $this->addSql('ALTER TABLE serie DROP image_id');
        $this->addSql('DROP INDEX UNIQ_9CE8D5463DA5256D');
        $this->addSql('ALTER TABLE publisher RENAME COLUMN image_id TO logo_id');
        $this->addSql('ALTER TABLE publisher ADD CONSTRAINT fk_9ce8d546f98f144a FOREIGN KEY (logo_id) REFERENCES publisher_logo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX uniq_9ce8d546f98f144a ON publisher (logo_id)');
    }
}
