<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210328005128 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create issues, authors, and theirs relations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE author (id UUID NOT NULL, name VARCHAR(255) NOT NULL, presentation TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN author.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE author_role (id UUID NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN author_role.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE issue (id UUID NOT NULL, title VARCHAR(255) NOT NULL, description TEXT NOT NULL DEFAULT \'\', cover VARCHAR(255) DEFAULT NULL, number INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN issue.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE issue_author_relation (id UUID NOT NULL, issue_id UUID NOT NULL, author_id UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_9556994F5E7AA58C ON issue_author_relation (issue_id)');
        $this->addSql('CREATE INDEX IDX_9556994FF675F31B ON issue_author_relation (author_id)');
        $this->addSql('COMMENT ON COLUMN issue_author_relation.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN issue_author_relation.issue_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN issue_author_relation.author_id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE issue_author_relation_author_role (issue_author_relation_id UUID NOT NULL, author_role_id UUID NOT NULL, PRIMARY KEY(issue_author_relation_id, author_role_id))');
        $this->addSql('CREATE INDEX IDX_399EA6653F0CCACA ON issue_author_relation_author_role (issue_author_relation_id)');
        $this->addSql('CREATE INDEX IDX_399EA6659339BDEF ON issue_author_relation_author_role (author_role_id)');
        $this->addSql('COMMENT ON COLUMN issue_author_relation_author_role.issue_author_relation_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN issue_author_relation_author_role.author_role_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE issue_author_relation ADD CONSTRAINT FK_9556994F5E7AA58C FOREIGN KEY (issue_id) REFERENCES issue (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_author_relation ADD CONSTRAINT FK_9556994FF675F31B FOREIGN KEY (author_id) REFERENCES author (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_author_relation_author_role ADD CONSTRAINT FK_399EA6653F0CCACA FOREIGN KEY (issue_author_relation_id) REFERENCES issue_author_relation (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_author_relation_author_role ADD CONSTRAINT FK_399EA6659339BDEF FOREIGN KEY (author_role_id) REFERENCES author_role (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE issue_author_relation DROP CONSTRAINT FK_9556994FF675F31B');
        $this->addSql('ALTER TABLE issue_author_relation_author_role DROP CONSTRAINT FK_399EA6659339BDEF');
        $this->addSql('ALTER TABLE issue_author_relation DROP CONSTRAINT FK_9556994F5E7AA58C');
        $this->addSql('ALTER TABLE issue_author_relation_author_role DROP CONSTRAINT FK_399EA6653F0CCACA');
        $this->addSql('DROP TABLE author');
        $this->addSql('DROP TABLE author_role');
        $this->addSql('DROP TABLE issue');
        $this->addSql('DROP TABLE issue_author_relation');
        $this->addSql('DROP TABLE issue_author_relation_author_role');
    }
}
