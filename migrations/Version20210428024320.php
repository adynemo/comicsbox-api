<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210428024320 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add createdAt and updatedAt for several entities and improve Issue';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE author ADD created_at DATE NOT NULL');
        $this->addSql('ALTER TABLE author ADD updated_at DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE author_role ADD created_at DATE NOT NULL');
        $this->addSql('ALTER TABLE author_role ADD updated_at DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE issue ADD page_count INT DEFAULT NULL');
        $this->addSql('ALTER TABLE issue ADD release_date DATE NOT NULL');
        $this->addSql('ALTER TABLE issue ADD created_at DATE NOT NULL');
        $this->addSql('ALTER TABLE issue ADD updated_at DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE publisher ADD created_at DATE NOT NULL');
        $this->addSql('ALTER TABLE publisher ADD updated_at DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE serie ADD created_at DATE NOT NULL');
        $this->addSql('ALTER TABLE serie ADD updated_at DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" ADD created_at DATE NOT NULL');
        $this->addSql('ALTER TABLE "user" ADD updated_at DATE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE "user" DROP created_at');
        $this->addSql('ALTER TABLE "user" DROP updated_at');
        $this->addSql('ALTER TABLE author DROP created_at');
        $this->addSql('ALTER TABLE author DROP updated_at');
        $this->addSql('ALTER TABLE author_role DROP created_at');
        $this->addSql('ALTER TABLE author_role DROP updated_at');
        $this->addSql('ALTER TABLE issue DROP page_count');
        $this->addSql('ALTER TABLE issue DROP release_date');
        $this->addSql('ALTER TABLE issue DROP created_at');
        $this->addSql('ALTER TABLE issue DROP updated_at');
        $this->addSql('ALTER TABLE publisher DROP created_at');
        $this->addSql('ALTER TABLE publisher DROP updated_at');
        $this->addSql('ALTER TABLE serie DROP created_at');
        $this->addSql('ALTER TABLE serie DROP updated_at');
    }
}
