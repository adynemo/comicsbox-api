<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220107040146 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Refactor nullable values';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE avatar ALTER file_path SET NOT NULL');
        $this->addSql('ALTER TABLE book ALTER serie_id DROP NOT NULL');
        $this->addSql('ALTER TABLE cover ALTER file_path SET NOT NULL');
        $this->addSql('ALTER TABLE logo ALTER file_path SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE cover ALTER file_path DROP NOT NULL');
        $this->addSql('ALTER TABLE avatar ALTER file_path DROP NOT NULL');
        $this->addSql('ALTER TABLE logo ALTER file_path DROP NOT NULL');
        $this->addSql('ALTER TABLE book ALTER serie_id SET NOT NULL');
    }
}
