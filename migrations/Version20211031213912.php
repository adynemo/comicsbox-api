<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211031213912 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add relations between issue/issue and character/character. Create Collection and more improvements';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE issue_repositories (book_source UUID NOT NULL, book_target UUID NOT NULL, PRIMARY KEY(book_source, book_target))');
        $this->addSql('CREATE INDEX IDX_D6FB13E5765D74FE ON issue_repositories (book_source)');
        $this->addSql('CREATE INDEX IDX_D6FB13E56FB82471 ON issue_repositories (book_target)');
        $this->addSql('CREATE TABLE collection (id UUID NOT NULL, publisher_id UUID NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FC4D653240C86FCE ON collection (publisher_id)');
        $this->addSql('ALTER TABLE issue_repositories ADD CONSTRAINT FK_D6FB13E5765D74FE FOREIGN KEY (book_source) REFERENCES book (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_repositories ADD CONSTRAINT FK_D6FB13E56FB82471 FOREIGN KEY (book_target) REFERENCES book (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE collection ADD CONSTRAINT FK_FC4D653240C86FCE FOREIGN KEY (publisher_id) REFERENCES publisher (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE issue_tpb');
        $this->addSql('ALTER TABLE book ADD original_book_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE book ADD collection_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE book ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE book ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE book ALTER image_id TYPE UUID');
        $this->addSql('ALTER TABLE book ALTER image_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book ALTER country_id TYPE UUID');
        $this->addSql('ALTER TABLE book ALTER country_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book ALTER country_id SET NOT NULL');
        $this->addSql('ALTER TABLE book ALTER format_id TYPE UUID');
        $this->addSql('ALTER TABLE book ALTER format_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book ALTER format_id SET NOT NULL');
        $this->addSql('ALTER TABLE book ALTER serie_id TYPE UUID');
        $this->addSql('ALTER TABLE book ALTER serie_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book ADD CONSTRAINT FK_CBE5A33190221D76 FOREIGN KEY (original_book_id) REFERENCES book (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE book ADD CONSTRAINT FK_CBE5A331514956FD FOREIGN KEY (collection_id) REFERENCES collection (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_CBE5A33190221D76 ON book (original_book_id)');
        $this->addSql('CREATE INDEX IDX_CBE5A331514956FD ON book (collection_id)');
        $this->addSql('ALTER TABLE book_author_relation ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE book_author_relation ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_author_relation ALTER book_id TYPE UUID');
        $this->addSql('ALTER TABLE book_author_relation ALTER book_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_author_relation ALTER author_id TYPE UUID');
        $this->addSql('ALTER TABLE book_author_relation ALTER author_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_author_relation_author_role ALTER book_author_relation_id TYPE UUID');
        $this->addSql('ALTER TABLE book_author_relation_author_role ALTER book_author_relation_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_author_relation_author_role ALTER author_role_id TYPE UUID');
        $this->addSql('ALTER TABLE book_author_relation_author_role ALTER author_role_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_read ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE book_read ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_read ALTER reader_id TYPE UUID');
        $this->addSql('ALTER TABLE book_read ALTER reader_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_read ALTER book_id TYPE UUID');
        $this->addSql('ALTER TABLE book_read ALTER book_id DROP DEFAULT');
        $this->addSql('ALTER TABLE "character" ADD main_character_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE "character" ALTER publisher_id TYPE UUID');
        $this->addSql('ALTER TABLE "character" ALTER publisher_id DROP DEFAULT');
        $this->addSql('ALTER TABLE "character" ALTER creators_id TYPE UUID');
        $this->addSql('ALTER TABLE "character" ALTER creators_id DROP DEFAULT');
        $this->addSql('ALTER TABLE "character" ADD CONSTRAINT FK_937AB03447F23211 FOREIGN KEY (first_appearance_id) REFERENCES book (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "character" ADD CONSTRAINT FK_937AB03488E0BCC FOREIGN KEY (main_character_id) REFERENCES character (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_937AB03488E0BCC ON "character" (main_character_id)');
        $this->addSql('ALTER TABLE character_book ALTER character_id TYPE UUID');
        $this->addSql('ALTER TABLE character_book ALTER character_id DROP DEFAULT');
        $this->addSql('ALTER TABLE character_book ALTER book_id TYPE UUID');
        $this->addSql('ALTER TABLE character_book ALTER book_id DROP DEFAULT');
        $this->addSql('ALTER TABLE country ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE country ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE format ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE format ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE publisher ADD country_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE publisher ADD CONSTRAINT FK_9CE8D546F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9CE8D546F92F3E70 ON publisher (country_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE book DROP CONSTRAINT FK_CBE5A331514956FD');
        $this->addSql('CREATE TABLE issue_tpb (book_source UUID NOT NULL, book_target UUID NOT NULL, PRIMARY KEY(book_source, book_target))');
        $this->addSql('CREATE INDEX idx_afbebd21765d74fe ON issue_tpb (book_source)');
        $this->addSql('CREATE INDEX idx_afbebd216fb82471 ON issue_tpb (book_target)');
        $this->addSql('ALTER TABLE issue_tpb ADD CONSTRAINT fk_afbebd21765d74fe FOREIGN KEY (book_source) REFERENCES book (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_tpb ADD CONSTRAINT fk_afbebd216fb82471 FOREIGN KEY (book_target) REFERENCES book (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE issue_repositories');
        $this->addSql('DROP TABLE collection');
        $this->addSql('ALTER TABLE publisher DROP CONSTRAINT FK_9CE8D546F92F3E70');
        $this->addSql('DROP INDEX IDX_9CE8D546F92F3E70');
        $this->addSql('ALTER TABLE publisher DROP country_id');
        $this->addSql('ALTER TABLE character DROP CONSTRAINT FK_937AB03447F23211');
        $this->addSql('ALTER TABLE character DROP CONSTRAINT FK_937AB03488E0BCC');
        $this->addSql('DROP INDEX IDX_937AB03488E0BCC');
        $this->addSql('ALTER TABLE character DROP main_character_id');
        $this->addSql('ALTER TABLE character ALTER publisher_id TYPE UUID');
        $this->addSql('ALTER TABLE character ALTER publisher_id DROP DEFAULT');
        $this->addSql('ALTER TABLE character ALTER creators_id TYPE UUID');
        $this->addSql('ALTER TABLE character ALTER creators_id DROP DEFAULT');
        $this->addSql('ALTER TABLE country ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE country ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE format ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE format ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_author_relation ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE book_author_relation ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_author_relation ALTER book_id TYPE UUID');
        $this->addSql('ALTER TABLE book_author_relation ALTER book_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_author_relation ALTER author_id TYPE UUID');
        $this->addSql('ALTER TABLE book_author_relation ALTER author_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_author_relation_author_role ALTER book_author_relation_id TYPE UUID');
        $this->addSql('ALTER TABLE book_author_relation_author_role ALTER book_author_relation_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_author_relation_author_role ALTER author_role_id TYPE UUID');
        $this->addSql('ALTER TABLE book_author_relation_author_role ALTER author_role_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_read ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE book_read ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_read ALTER reader_id TYPE UUID');
        $this->addSql('ALTER TABLE book_read ALTER reader_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book_read ALTER book_id TYPE UUID');
        $this->addSql('ALTER TABLE book_read ALTER book_id DROP DEFAULT');
        $this->addSql('ALTER TABLE character_book ALTER character_id TYPE UUID');
        $this->addSql('ALTER TABLE character_book ALTER character_id DROP DEFAULT');
        $this->addSql('ALTER TABLE character_book ALTER book_id TYPE UUID');
        $this->addSql('ALTER TABLE character_book ALTER book_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book DROP CONSTRAINT FK_CBE5A33190221D76');
        $this->addSql('DROP INDEX IDX_CBE5A33190221D76');
        $this->addSql('DROP INDEX IDX_CBE5A331514956FD');
        $this->addSql('ALTER TABLE book DROP original_book_id');
        $this->addSql('ALTER TABLE book DROP collection_id');
        $this->addSql('ALTER TABLE book ALTER id TYPE UUID');
        $this->addSql('ALTER TABLE book ALTER id DROP DEFAULT');
        $this->addSql('ALTER TABLE book ALTER image_id TYPE UUID');
        $this->addSql('ALTER TABLE book ALTER image_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book ALTER country_id TYPE UUID');
        $this->addSql('ALTER TABLE book ALTER country_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book ALTER country_id DROP NOT NULL');
        $this->addSql('ALTER TABLE book ALTER format_id TYPE UUID');
        $this->addSql('ALTER TABLE book ALTER format_id DROP DEFAULT');
        $this->addSql('ALTER TABLE book ALTER format_id DROP NOT NULL');
        $this->addSql('ALTER TABLE book ALTER serie_id TYPE UUID');
        $this->addSql('ALTER TABLE book ALTER serie_id DROP DEFAULT');
    }
}
