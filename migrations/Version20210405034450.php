<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210405034450 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add cover file to issue';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE cover (id UUID NOT NULL, file_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN cover.id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE issue ADD cover_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE issue DROP cover');
        $this->addSql('COMMENT ON COLUMN issue.cover_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233E922726E9 FOREIGN KEY (cover_id) REFERENCES cover (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_12AD233E922726E9 ON issue (cover_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE issue DROP CONSTRAINT FK_12AD233E922726E9');
        $this->addSql('DROP TABLE cover');
        $this->addSql('DROP INDEX IDX_12AD233E922726E9');
        $this->addSql('ALTER TABLE issue ADD cover VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE issue DROP cover_id');
    }
}
