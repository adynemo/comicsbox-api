<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211030043114 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add Country and Format and improve Author, Character and Issue.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE country (id UUID NOT NULL, name VARCHAR(255) NOT NULL, code VARCHAR(2) NOT NULL, currency VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE format (id UUID NOT NULL, label VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE author ADD birth_date DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE author ADD death_date DATE DEFAULT NULL');
        $this->addSql('ALTER TABLE "character" ADD publisher_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE "character" ADD creators_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE "character" ADD history TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE "character" ADD CONSTRAINT FK_937AB03440C86FCE FOREIGN KEY (publisher_id) REFERENCES publisher (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE "character" ADD CONSTRAINT FK_937AB034D8A599F6 FOREIGN KEY (creators_id) REFERENCES author (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_937AB03440C86FCE ON "character" (publisher_id)');
        $this->addSql('CREATE INDEX IDX_937AB034D8A599F6 ON "character" (creators_id)');
        $this->addSql('ALTER TABLE issue ADD country_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE issue ADD format_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE issue ADD isbn VARCHAR(13) DEFAULT NULL');
        $this->addSql('ALTER TABLE issue ADD sku VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE issue ADD price DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233EF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233ED629F605 FOREIGN KEY (format_id) REFERENCES format (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_12AD233EF92F3E70 ON issue (country_id)');
        $this->addSql('CREATE INDEX IDX_12AD233ED629F605 ON issue (format_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE issue DROP CONSTRAINT FK_12AD233EF92F3E70');
        $this->addSql('ALTER TABLE issue DROP CONSTRAINT FK_12AD233ED629F605');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE format');
        $this->addSql('ALTER TABLE character DROP CONSTRAINT FK_937AB03440C86FCE');
        $this->addSql('ALTER TABLE character DROP CONSTRAINT FK_937AB034D8A599F6');
        $this->addSql('DROP INDEX IDX_937AB03440C86FCE');
        $this->addSql('DROP INDEX IDX_937AB034D8A599F6');
        $this->addSql('ALTER TABLE character DROP publisher_id');
        $this->addSql('ALTER TABLE character DROP creators_id');
        $this->addSql('ALTER TABLE character DROP history');
        $this->addSql('ALTER TABLE author DROP birth_date');
        $this->addSql('ALTER TABLE author DROP death_date');
        $this->addSql('DROP INDEX IDX_12AD233EF92F3E70');
        $this->addSql('DROP INDEX IDX_12AD233ED629F605');
        $this->addSql('ALTER TABLE issue DROP country_id');
        $this->addSql('ALTER TABLE issue DROP format_id');
        $this->addSql('ALTER TABLE issue DROP isbn');
        $this->addSql('ALTER TABLE issue DROP sku');
        $this->addSql('ALTER TABLE issue DROP price');
    }
}
