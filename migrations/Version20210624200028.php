<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210624200028 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add avatar to authors';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE author ADD avatar_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN author.avatar_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE author ADD CONSTRAINT FK_BDAFD8C886383B10 FOREIGN KEY (avatar_id) REFERENCES avatar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BDAFD8C886383B10 ON author (avatar_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE author DROP CONSTRAINT FK_BDAFD8C886383B10');
        $this->addSql('DROP INDEX UNIQ_BDAFD8C886383B10');
        $this->addSql('ALTER TABLE author DROP avatar_id');
    }
}
