<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210424050744 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create Serie, Publisher and relation between Serie and Issue';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE publisher (id UUID NOT NULL, name VARCHAR(255) NOT NULL, description TEXT NOT NULL DEFAULT \'\', foundation_year INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN publisher.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE serie (id UUID NOT NULL, publisher_id UUID NOT NULL, title VARCHAR(255) NOT NULL, begin_year INT NOT NULL, end_year INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AA3A933440C86FCE ON serie (publisher_id)');
        $this->addSql('COMMENT ON COLUMN serie.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN serie.publisher_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE serie ADD CONSTRAINT FK_AA3A933440C86FCE FOREIGN KEY (publisher_id) REFERENCES publisher (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue ADD serie_id UUID NOT NULL');
        $this->addSql('COMMENT ON COLUMN issue.serie_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE issue ADD CONSTRAINT FK_12AD233ED94388BD FOREIGN KEY (serie_id) REFERENCES serie (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_12AD233ED94388BD ON issue (serie_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE serie DROP CONSTRAINT FK_AA3A933440C86FCE');
        $this->addSql('ALTER TABLE issue DROP CONSTRAINT FK_12AD233ED94388BD');
        $this->addSql('DROP TABLE publisher');
        $this->addSql('DROP TABLE serie');
        $this->addSql('DROP INDEX IDX_12AD233ED94388BD');
        $this->addSql('ALTER TABLE issue DROP serie_id');
    }
}
