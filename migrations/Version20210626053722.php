<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210626053722 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add logo to publishers, refactor authors avatar and add description to serie';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE author_avatar (id UUID NOT NULL, file_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN author_avatar.id IS \'(DC2Type:uuid)\'');
        $this->addSql('CREATE TABLE publisher_logo (id UUID NOT NULL, file_path VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN publisher_logo.id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE author DROP CONSTRAINT FK_BDAFD8C886383B10');
        $this->addSql('ALTER TABLE author ADD CONSTRAINT FK_BDAFD8C886383B10 FOREIGN KEY (avatar_id) REFERENCES author_avatar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE publisher ADD logo_id UUID DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN publisher.logo_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE publisher ADD CONSTRAINT FK_9CE8D546F98F144A FOREIGN KEY (logo_id) REFERENCES publisher_logo (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9CE8D546F98F144A ON publisher (logo_id)');
        $this->addSql('ALTER TABLE serie ADD description TEXT NOT NULL DEFAULT \'\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE author DROP CONSTRAINT FK_BDAFD8C886383B10');
        $this->addSql('ALTER TABLE publisher DROP CONSTRAINT FK_9CE8D546F98F144A');
        $this->addSql('DROP TABLE author_avatar');
        $this->addSql('DROP TABLE publisher_logo');
        $this->addSql('ALTER TABLE author DROP CONSTRAINT fk_bdafd8c886383b10');
        $this->addSql('ALTER TABLE author ADD CONSTRAINT fk_bdafd8c886383b10 FOREIGN KEY (avatar_id) REFERENCES avatar (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP INDEX UNIQ_9CE8D546F98F144A');
        $this->addSql('ALTER TABLE publisher DROP logo_id');
        $this->addSql('ALTER TABLE serie DROP description');
    }
}
