<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211109024059 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add ManyToMany relation between Character and Author and add Country to Author';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE character_author (character_id UUID NOT NULL, author_id UUID NOT NULL, PRIMARY KEY(character_id, author_id))');
        $this->addSql('CREATE INDEX IDX_1EED1B5B1136BE75 ON character_author (character_id)');
        $this->addSql('CREATE INDEX IDX_1EED1B5BF675F31B ON character_author (author_id)');
        $this->addSql('ALTER TABLE character_author ADD CONSTRAINT FK_1EED1B5B1136BE75 FOREIGN KEY (character_id) REFERENCES character (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE character_author ADD CONSTRAINT FK_1EED1B5BF675F31B FOREIGN KEY (author_id) REFERENCES author (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE author ADD country_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE author ADD CONSTRAINT FK_BDAFD8C8F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_BDAFD8C8F92F3E70 ON author (country_id)');
        $this->addSql('ALTER TABLE "character" DROP CONSTRAINT fk_937ab034d8a599f6');
        $this->addSql('DROP INDEX idx_937ab034d8a599f6');
        $this->addSql('ALTER TABLE "character" DROP creators_id');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE character_author');
        $this->addSql('ALTER TABLE author DROP CONSTRAINT FK_BDAFD8C8F92F3E70');
        $this->addSql('DROP INDEX IDX_BDAFD8C8F92F3E70');
        $this->addSql('ALTER TABLE author DROP country_id');
        $this->addSql('ALTER TABLE character ADD creators_id UUID DEFAULT NULL');
        $this->addSql('ALTER TABLE character ADD CONSTRAINT fk_937ab034d8a599f6 FOREIGN KEY (creators_id) REFERENCES author (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_937ab034d8a599f6 ON character (creators_id)');
    }
}
