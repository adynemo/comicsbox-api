<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210917014330 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create IssueRead';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE issue_read (id UUID NOT NULL, reader_id UUID NOT NULL, issue_id UUID NOT NULL, count INT DEFAULT 1 NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C0B089AB1717D737 ON issue_read (reader_id)');
        $this->addSql('CREATE INDEX IDX_C0B089AB5E7AA58C ON issue_read (issue_id)');
        $this->addSql('COMMENT ON COLUMN issue_read.id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN issue_read.reader_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN issue_read.issue_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE issue_read ADD CONSTRAINT FK_C0B089AB1717D737 FOREIGN KEY (reader_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE issue_read ADD CONSTRAINT FK_C0B089AB5E7AA58C FOREIGN KEY (issue_id) REFERENCES issue (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE issue_read');
    }
}
