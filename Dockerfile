FROM php:8.0

RUN useradd -ms /bin/bash cb

RUN apt update && apt upgrade -y && \
    apt install -y wget make unzip nano curl sudo git

COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions gd xdebug intl zip pgsql pdo_pgsql zip

COPY docker/php/comicsbox.ini "$PHP_INI_DIR/conf.d/comicsbox.ini"

COPY --from=composer:2.0 /usr/bin/composer /usr/bin/composer

RUN wget https://get.symfony.com/cli/installer -O - | bash && \
    mv $HOME/.symfony/bin/symfony /usr/local/bin/symfony

RUN echo "cb  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/cb

USER cb

WORKDIR /app

EXPOSE 8080
