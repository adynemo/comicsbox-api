<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Book;
use App\Repository\BookRepository;
use App\Service\SlugConverter;
use App\Service\StringModifier;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class BookSubscriber implements EventSubscriberInterface
{
    private const WRITE_METHODS = [Request::METHOD_POST, Request::METHOD_PATCH, Request::METHOD_PUT];

    public function __construct(
        private SlugConverter $slugifier,
        private StringModifier $stringModifier,
        private BookRepository $repository
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['process', EventPriorities::PRE_WRITE],
        ];
    }

    public function process(ViewEvent $event): void
    {
        $book = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$book instanceof Book || !in_array($method, self::WRITE_METHODS)) {
            return;
        }

        if ($book->getIsbn()) {
            $isbn = $this->stringModifier->clearISBN($book->getIsbn());
            $book->setIsbn($isbn);
        }

        if (Request::METHOD_POST !== $method) {
            return;
        }

        $title = $book->getTitle();
        $slug = $this->slugifier->slugify($title);
        do {
            $isUnique = null === $this->repository->findOneBy(['slug' => $slug]);
            if (!$isUnique) {
                $slug = $this->slugifier->increment();
            }
        } while (false === $isUnique);

        $book->setSlug($slug);
    }
}
