<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Book;
use App\Entity\BookAuthorRelation;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * This subscriber is necessary to resolve a bug with the denormalization.
 * https://stackoverflow.com/questions/70817654/post-never-create-embedded-relation-on-onetomany
 * https://github.com/api-platform/api-platform/issues/1538.
 */
class DenormalizeSubscriber implements EventSubscriberInterface
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['denormalizeRelations', EventPriorities::PRE_WRITE],
        ];
    }

    public function denormalizeRelations(ViewEvent $event): void
    {
        $object = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        $allowedMethods = [Request::METHOD_POST, Request::METHOD_PATCH, Request::METHOD_PUT];
        if (!in_array($method, $allowedMethods)) {
            return;
        }

        if ($object instanceof Book) {
            /** @var string $data */
            $data = $event->getRequest()->getContent();
            $data = json_decode($data);
            $authors = $data->authors;
            foreach ($authors as $author) {
                $relation = $this->serializer->deserialize(json_encode($author), BookAuthorRelation::class, 'json');
                $object->addAuthor($relation);
            }
        }
    }
}
