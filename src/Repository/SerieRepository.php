<?php

namespace App\Repository;

use App\Entity\Author;
use App\Entity\Character;
use App\Entity\Serie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Serie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Serie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Serie[]    findAll()
 * @method Serie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SerieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Serie::class);
    }

    public function findSerieByAuthor(Author $author): array
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.books', 'i', Join::WITH, 'i.serie = s')
            ->join('i.authors', 'a')
            ->where('a.author = :author')
            ->setParameter('author', $author)
            ->getQuery()
            ->getResult()
            ;
    }

    public function findSerieByCharacter(Character $character): array
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.books', 'i', Join::WITH, 'i.serie = s')
            ->join('i.characters', 'c')
            ->where('c = :character')
            ->setParameter('character', $character)
            ->getQuery()
            ->getResult()
            ;
    }
}
