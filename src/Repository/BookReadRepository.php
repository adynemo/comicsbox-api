<?php

namespace App\Repository;

use App\Entity\BookRead;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BookRead|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookRead|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookRead[]    findAll()
 * @method BookRead[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookReadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BookRead::class);
    }

    public function findByBooks(User $reader, array $books): array
    {
        $booksId = array_map(function ($book) {
            return $book->getId();
        }, $books);

        return $this->createQueryBuilder('r')
            ->where('r.reader = :reader')
            ->andWhere('r.book IN (:books)')
            ->setParameters([
                'reader' => $reader,
                'books' => $booksId,
            ])
            ->getQuery()
            ->getResult()
            ;
    }
}
