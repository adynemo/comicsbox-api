<?php

namespace App\Repository;

use App\Entity\BookAuthorRelation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BookAuthorRelation|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookAuthorRelation|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookAuthorRelation[]    findAll()
 * @method BookAuthorRelation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookAuthorRelationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BookAuthorRelation::class);
    }
}
