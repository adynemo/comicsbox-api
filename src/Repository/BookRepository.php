<?php

namespace App\Repository;

use App\Entity\Book;
use App\Entity\Serie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    public function findPreviousBook(Book $book): ?Book
    {
        return $this->createQueryBuilder('i')
            ->where('i.serie = :serie')
            ->andWhere('i.releaseDate < :releaseDate')
            ->setParameters([
                'serie' => $book->getSerie(),
                'releaseDate' => $book->getReleaseDate(),
            ])
            ->orderBy('i.releaseDate', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findNextBook(Book $book): ?Book
    {
        return $this->createQueryBuilder('i')
            ->where('i.serie = :serie')
            ->andWhere('i.releaseDate > :releaseDate')
            ->setParameters([
                'serie' => $book->getSerie(),
                'releaseDate' => $book->getReleaseDate(),
            ])
            ->orderBy('i.releaseDate', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
            ;
    }

    public function findBooksBySerie(Serie $serie): array
    {
        return $this->createQueryBuilder('i')
            ->where('i.serie = :serie')
            ->orderBy('i.releaseDate', 'ASC')
            ->setParameter('serie', $serie)
            ->getQuery()
            ->getResult()
            ;
    }
}
