<?php

namespace App\Service;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Publisher;
use App\Entity\Serie;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;

class StatsCalculator
{
    public function __construct(
        private EntityManagerInterface $entityManager
    ) {
    }

    public function getAllStats(): array
    {
        $stats = [];
        $entities = [
            Author::class,
            Book::class,
            Serie::class,
            Publisher::class,
            User::class,
        ];

        foreach ($entities as $entity) {
            $name = substr($entity, strrpos($entity, '\\') + 1);
            $stats[$name] = $this->getTotalCount($entity);
        }

        return $stats;
    }

    private function getTotalCount(string $from): int
    {
        $result = $this->entityManager
            ->createQueryBuilder()
            ->from($from, 'f')
            ->select('COUNT(f) as count')
            ->getQuery()
            ->getOneOrNullResult();

        return $result ? $result['count'] : 0;
    }
}
