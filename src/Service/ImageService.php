<?php

namespace App\Service;

use App\Entity\Abstracts\Image;
use App\Entity\Interfaces\EntityWithImageInterface;
use Vich\UploaderBundle\Storage\StorageInterface;

class ImageService
{
    public function __construct(private StorageInterface $storage)
    {
    }

    public function setImageUrl(Image|EntityWithImageInterface|null $object): void
    {
        $image = $object instanceof EntityWithImageInterface ? $object->getImage() : $object;
        if ($image instanceof Image) {
            $image->setUrl($this->storage->resolveUri($image, 'file'));
        }
    }

    public function setImageUrlFromIterable(iterable $items): void
    {
        foreach ($items as $item) {
            $this->setImageUrl($item);
        }
    }
}
