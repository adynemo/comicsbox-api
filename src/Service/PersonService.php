<?php

namespace App\Service;

use App\DAO\Serie as SerieDAO;
use App\Entity\Abstracts\Person;
use App\Entity\Author;
use App\Entity\Character;
use App\Entity\Serie;
use App\Repository\SerieRepository;
use App\Transformer\SerieTransformer;
use Doctrine\Persistence\ManagerRegistry;
use JetBrains\PhpStorm\ArrayShape;

class PersonService
{
    public function __construct(
        private ManagerRegistry $registry,
        private ImageService $imageService,
        private SerieTransformer $serieTransformer
    ) {
    }

    #[ArrayShape(['person' => Person::class, 'series' => [SerieDAO::class]])]
    public function convertPersonForProxyAPI(Person $person): array
    {
        /** @var SerieRepository $repository */
        $repository = $this->registry->getRepository(Serie::class);
        if ($person instanceof Author) {
            $results = $repository->findSerieByAuthor($person);
        } elseif ($person instanceof Character) {
            $results = $repository->findSerieByCharacter($person);
        } else {
            throw new \RuntimeException('You must provide an Author or a Character');
        }

        $series = [];
        foreach ($results as $result) {
            $this->imageService->setImageUrl($result);
            $series[] = $this->serieTransformer->transform($result);
        }

        $this->imageService->setImageUrl($person);

        return [
            'person' => $person,
            'series' => $series,
        ];
    }
}
