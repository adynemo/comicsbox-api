<?php

namespace App\Service;

class StringModifier
{
    public function clearISBN(string $input): string
    {
        return preg_replace('/\D/', '', $input);
    }
}
