<?php

namespace App\Service;

class SlugConverter
{
    private const DIVIDER = '-';

    private string $slug;
    private int $number = 0;

    public function slugify(string $title): string
    {
        // replace non letter or digits by divider
        $this->slug = preg_replace('~[^\pL]+~u', self::DIVIDER, $title);

        // transliterate
        $this->slug = iconv('utf-8', 'us-ascii//TRANSLIT', $this->slug);

        // remove unwanted characters
        $this->slug = preg_replace('~[^-\w]+~', '', $this->slug);

        // trim
        $this->slug = trim($this->slug, self::DIVIDER);

        // remove duplicate divider
        $this->slug = preg_replace('~-+~', self::DIVIDER, $this->slug);

        // lowercase
        $this->slug = strtolower($this->slug);

        if (empty($this->slug)) {
            return 'n-a';
        }

        return $this->slug;
    }

    public function increment(): string
    {
        ++$this->number;

        return $this->slug.self::DIVIDER.$this->number;
    }
}
