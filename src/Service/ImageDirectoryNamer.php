<?php

namespace App\Service;

use App\Entity\Abstracts\Image;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\DirectoryNamerInterface;

class ImageDirectoryNamer implements DirectoryNamerInterface
{
    public function directoryName($object, PropertyMapping $mapping): string
    {
        if (!$object instanceof Image) {
            throw new \RuntimeException(sprintf('You must pass %s, %s passed', Image::class, get_class($object)));
        }

        return $this->generatePath($object->getFilePath());
    }

    private function generatePath(string $filename): string
    {
        $filename = bin2hex((string) preg_replace('/[a-z]*/i', '', md5($filename)));

        return sprintf(
            '%d/%d/',
            substr($filename, 0, 2),
            substr($filename, -2)
        );
    }
}
