<?php

namespace App\Service;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;

class ImageOptimizer
{
    private Imagine $imagine;

    public function __construct()
    {
        $this->imagine = new Imagine();
    }

    public function resize(string $filename, int $width, int $height): void
    {
        $imageSize = getimagesize($filename);
        if (!$imageSize) {
            throw new \RuntimeException('Unable to get image size.');
        }

        [$iWidth, $iHeight] = $imageSize;
        $ratio = $iWidth / $iHeight;
        if ($width / $height > $ratio) {
            $width = $height * $ratio;
        } else {
            $height = (int) round($width / $ratio);
        }

        $photo = $this->imagine->open($filename);
        $photo->resize(new Box($width, $height))->save($filename);
    }
}
