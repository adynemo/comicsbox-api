<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Abstracts\Image;
use App\Entity\Interfaces\EntityWithImageInterface;
use App\Entity\Traits\DateActionTrait;
use App\Repository\PublisherRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=PublisherRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'post',
    ],
    itemOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'put',
        'delete',
        'patch',
    ],
    attributes: ['security' => 'is_granted("ROLE_ADMIN")'],
    normalizationContext: ['groups' => ['get:publisher']],
)]
class Publisher implements EntityWithImageInterface
{
    use DateActionTrait;

    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator")
     */
    #[Groups(['get:serie', 'get:publisher'])]
    private Uuid $id;

    /**
     * @ORM\Column
     */
    #[Groups(['get:serie', 'get:publisher'])]
    private string $name;

    /**
     * @ORM\Column(type="text", options={"default": ""})
     */
    #[Groups(['get:publisher'])]
    private string $description = '';

    /**
     * @ORM\Column(type="integer")
     */
    #[Groups(['get:publisher'])]
    private int $foundationYear;

    /**
     * @ORM\OneToMany(targetEntity=Serie::class, mappedBy="publisher", orphanRemoval=true)
     */
    #[Groups(['get:publisher'])]
    private Collection $series;

    /**
     * @ORM\OneToOne(targetEntity=Logo::class, inversedBy="publisher", cascade={"persist", "remove"})
     */
    #[Groups(['get:publisher'])]
    private ?Logo $image;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    #[Groups(['get:publisher'])]
    private ?int $endYear;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    #[Groups(['get:publisher'])]
    private ?string $url;

    /**
     * @ORM\OneToMany(targetEntity=Character::class, mappedBy="publisher")
     */
    #[Groups(['get:publisher'])]
    private Collection $characters;

    /**
     * @ORM\OneToMany(targetEntity=PublisherCollection::class, mappedBy="publisher", orphanRemoval=true)
     */
    #[Groups(['get:publisher'])]
    private Collection $collections;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="publishers")
     */
    #[Groups(['get:publisher'])]
    private ?Country $country;

    public function __construct()
    {
        $this->series = new ArrayCollection();
        $this->characters = new ArrayCollection();
        $this->collections = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getFoundationYear(): int
    {
        return $this->foundationYear;
    }

    public function setFoundationYear(int $foundationYear): self
    {
        $this->foundationYear = $foundationYear;

        return $this;
    }

    /**
     * @return Collection|Serie[]
     */
    public function getSeries(): Collection
    {
        return $this->series;
    }

    public function getImage(): ?Logo
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        if (!$image instanceof Logo) {
            throw new InvalidTypeException('Image must be an instance of '.Logo::class);
        }
        $this->image = $image;

        return $this;
    }

    public function getEndYear(): ?int
    {
        return $this->endYear;
    }

    public function setEndYear(?int $endYear): self
    {
        $this->endYear = $endYear;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return Collection|Character[]
     */
    public function getCharacters(): Collection
    {
        return $this->characters;
    }

    public function addCharacter(Character $character): self
    {
        if (!$this->characters->contains($character)) {
            $this->characters[] = $character;
            $character->setPublisher($this);
        }

        return $this;
    }

    public function removeCharacter(Character $character): self
    {
        if ($this->characters->removeElement($character)) {
            // set the owning side to null (unless already changed)
            if ($character->getPublisher() === $this) {
                $character->setPublisher(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PublisherCollection[]
     */
    public function getCollections(): Collection
    {
        return $this->collections;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }
}
