<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Abstracts\Image;
use App\Entity\Interfaces\EntityWithImageInterface;
use App\Entity\Traits\DateActionTrait;
use App\Repository\SerieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

/**
 * todo: add "banner" field (+ to some other entities).
 *
 * @ORM\Entity(repositoryClass=SerieRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'post',
    ],
    itemOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'proxy_get' => [
            'method' => 'GET',
            'security' => 'is_granted("PUBLIC_ACCESS")',
            'route_name' => 'proxy_serie_get',
        ],
        'put',
        'delete',
        'patch',
    ],
    attributes: ['security' => 'is_granted("ROLE_ADMIN")'],
    normalizationContext: ['groups' => ['get:serie']],
)]
#[ApiFilter(SearchFilter::class, properties: [
    'books.characters' => 'exact',
    'books.authors.author' => 'exact',
])]
class Serie implements EntityWithImageInterface
{
    use DateActionTrait;

    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator")
     */
    #[Groups(['get:book', 'get:serie'])]
    private Uuid $id;

    /**
     * @ORM\Column
     */
    #[Groups(['get:book', 'get:serie'])]
    private string $title;

    /**
     * @ORM\Column(type="text", options={"default": ""})
     */
    #[Groups(['get:serie'])]
    private string $description = '';

    /**
     * @ORM\OneToMany(targetEntity=Book::class, mappedBy="serie", orphanRemoval=true)
     */
    #[Groups(['get:serie'])]
    private Collection $books;

    /**
     * @ORM\Column(type="integer")
     */
    #[Groups(['get:serie'])]
    private int $beginYear;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    #[Groups(['get:serie'])]
    private ?int $endYear;

    /**
     * @ORM\ManyToOne(targetEntity=Publisher::class, inversedBy="series")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Groups(['get:serie'])]
    private Publisher $publisher;

    /**
     * @ORM\OneToOne(targetEntity=Cover::class, inversedBy="serie", cascade={"persist", "remove"})
     */
    #[Groups(['get:serie'])]
    private ?Cover $image;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->setSerie($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->removeElement($book)) {
            // set the owning side to null (unless already changed)
            if ($book->getSerie() === $this) {
                $book->setSerie(null);
            }
        }

        return $this;
    }

    public function getBeginYear(): ?int
    {
        return $this->beginYear;
    }

    public function setBeginYear(int $beginYear): self
    {
        $this->beginYear = $beginYear;

        return $this;
    }

    public function getEndYear(): ?int
    {
        return $this->endYear;
    }

    public function setEndYear(?int $endYear): self
    {
        $this->endYear = $endYear;

        return $this;
    }

    public function getPublisher(): Publisher
    {
        return $this->publisher;
    }

    public function setPublisher(Publisher $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }

    public function getImage(): ?Cover
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        if (!$image instanceof Cover) {
            throw new InvalidTypeException('Image must be an instance of '.Cover::class);
        }
        $this->image = $image;

        return $this;
    }
}
