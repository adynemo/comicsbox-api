<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\BookAuthorRelationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=BookAuthorRelationRepository::class)
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'post',
    ],
    itemOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'put',
        'delete',
        'patch',
    ],
    attributes: ['security' => 'is_granted("ROLE_ADMIN")'],
    normalizationContext: ['groups' => ['book-author-roles']],
)]
#[ApiFilter(SearchFilter::class, properties: [
    'book.id' => 'exact',
    'book' => 'ipartial',
    'author' => 'ipartial',
])]
class BookAuthorRelation
{
    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator")
     */
    #[Groups(['book-author-roles'])]
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity=Book::class, inversedBy="authors")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Groups(['book-author-roles'])]
    private Book $book;

    /**
     * @ORM\ManyToOne(targetEntity=Author::class, inversedBy="books")
     * @ORM\JoinColumn(nullable=false)
     */
    #[Groups(['book-author-roles', 'get:book', 'post:book'])]
    private Author $author;

    /**
     * @ORM\ManyToMany(targetEntity=AuthorRole::class)
     */
    #[Groups(['book-author-roles', 'get:book', 'post:book'])]
    private Collection $roles;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getBook(): Book
    {
        return $this->book;
    }

    public function setBook(Book $book): self
    {
        $this->book = $book;

        return $this;
    }

    public function getAuthor(): Author
    {
        return $this->author;
    }

    public function setAuthor(Author $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|AuthorRole[]
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    public function addRole(AuthorRole $role): self
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole(AuthorRole $role): self
    {
        $this->roles->removeElement($role);

        return $this;
    }
}
