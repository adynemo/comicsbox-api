<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CreateImageController;
use App\Entity\Abstracts\Image;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'post' => [
            'controller' => CreateImageController::class,
            'deserialize' => false,
            'validation_groups' => ['Default', 'image_create'],
            'openapi_context' => [
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file' => [
                                        'type' => 'string',
                                        'format' => 'binary',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    itemOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'delete',
    ],
    attributes: ['security' => 'is_granted("ROLE_ADMIN")'],
    normalizationContext: ['groups' => ['image_read']],
)]
class Cover extends Image
{
    /**
     * @Assert\NotNull(groups={"image_create"})
     * @Vich\UploadableField(mapping="cover", fileNameProperty="filePath")
     */
    protected File $file;

    /**
     * @ORM\OneToOne(targetEntity=Book::class, mappedBy="image", cascade={"persist"})
     */
    #[Groups(['image_read'])]
    private ?Book $book;

    /**
     * @ORM\OneToOne(targetEntity=Serie::class, mappedBy="image", cascade={"persist"})
     */
    #[Groups(['image_read'])]
    private ?Serie $serie;

    public function getBook(): ?Book
    {
        return $this->book;
    }

    public function setBook(?Book $book): self
    {
        $this->checkRelation($book, 'book');

        $this->book = $book;

        return $this;
    }

    public function getSerie(): ?Serie
    {
        return $this->serie;
    }

    public function setSerie(?Serie $serie): self
    {
        $this->checkRelation($serie, 'serie');

        $this->serie = $serie;

        return $this;
    }

    /**
     * @ORM\PreRemove
     */
    public function updateRelations(): void
    {
        $this->book?->setImage(null);
        $this->serie?->setImage(null);
    }
}
