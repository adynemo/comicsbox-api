<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CreateUserController;
use App\Entity\Abstracts\Image;
use App\Entity\Interfaces\EntityWithImageInterface;
use App\Entity\Traits\DateActionTrait;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @ORM\HasLifecycleCallbacks()
 */
#[ApiResource(
    collectionOperations: [
        'get' => [
            'security' => 'is_granted("ROLE_USER")',
            'normalization_context' => ['groups' => ['user:get:own', 'user:get:other', 'admin:get']],
        ],
        'post' => [
            'security' => 'is_granted("PUBLIC_ACCESS")',
            'controller' => CreateUserController::class,
        ],
    ],
    itemOperations: [
        'get' => [
            'security' => 'is_granted("ROLE_USER")',
            'normalization_context' => ['groups' => ['user:get:own', 'user:get:other', 'admin:get']],
        ],
        'put' => ['security' => 'is_granted("ROLE_ADMIN") or object == user'],
        'delete' => ['security' => 'is_granted("ROLE_ADMIN") or object == user'],
        'patch' => ['security' => 'is_granted("ROLE_ADMIN") or object == user'],
    ],
    attributes: ['security' => 'is_granted("ROLE_USER")'],
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface, EntityWithImageInterface
{
    use DateActionTrait;

    public const BASE_URI = '/api/users';

    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator")
     */
    private Uuid $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    #[Groups(['user:get:own', 'user:get:other', 'admin:get'])]
    private string $username;

    /**
     * @ORM\Column(type="json")
     */
    #[Groups(['user:get:own', 'admin:get'])]
    private array $roles = [];

    /**
     * @ORM\Column(type="string")
     */
    #[Groups(['user:get:own', 'admin:get'])]
    private string $password;

    /**
     * @ORM\Column
     */
    #[Assert\Email(message: "The email '{{ value }}' is not a valid email.")]
    #[Groups(['user:get:own', 'admin:get'])]
    private string $email;

    /**
     * @ORM\OneToOne(targetEntity=Avatar::class, inversedBy="user", cascade={"persist", "remove"})
     */
    #[Groups(['user:get:own', 'user:get:other', 'admin:get'])]
    private ?Avatar $image;

    /**
     * @ORM\OneToMany(targetEntity=BookRead::class, mappedBy="reader", orphanRemoval=true)
     */
    #[Groups(['user:get:own'])]
    private Collection $bookReads;

    public function __construct()
    {
        $this->bookReads = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @deprecated
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @deprecated
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getImage(): ?Avatar
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        if (!$image instanceof Avatar) {
            throw new InvalidTypeException('Image must be an instance of '.Avatar::class);
        }
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|BookRead[]
     */
    public function getBookReads(): Collection
    {
        return $this->bookReads;
    }
}
