<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CreateImageController;
use App\Entity\Abstracts\Image;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks()
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'post' => [
            'controller' => CreateImageController::class,
            'deserialize' => false,
            'validation_groups' => ['Default', 'image_create'],
            'openapi_context' => [
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file' => [
                                        'type' => 'string',
                                        'format' => 'binary',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    itemOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'delete',
    ],
    attributes: ['security' => 'is_granted("ROLE_ADMIN")'],
    normalizationContext: ['groups' => ['image_read']],
)]
class Logo extends Image
{
    /**
     * @Assert\NotNull(groups={"image_create"})
     * @Vich\UploadableField(mapping="logo", fileNameProperty="filePath")
     */
    protected File $file;

    /**
     * @ORM\OneToOne(targetEntity=Publisher::class, mappedBy="image", cascade={"persist"})
     */
    #[Groups(['image_read'])]
    private ?Publisher $publisher;

    public function getPublisher(): ?Publisher
    {
        return $this->publisher;
    }

    public function setPublisher(?Publisher $publisher): self
    {
        $this->checkRelation($publisher, 'publisher');

        $this->publisher = $publisher;

        return $this;
    }

    /**
     * @ORM\PreRemove
     */
    public function updateRelations(): void
    {
        $this->publisher?->setImage(null);
    }
}
