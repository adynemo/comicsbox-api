<?php

namespace App\Entity\Interfaces;

use App\Entity\Abstracts\Image;

interface EntityWithImageInterface
{
    public function getImage(): ?Image;

    public function setImage(?Image $image): self;
}
