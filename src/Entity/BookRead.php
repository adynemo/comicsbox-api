<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\BookReadRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=BookReadRepository::class)
 * @UniqueEntity(fields={"reader", "book"}, message="One read between reader and book. Increment count field instead.")
 */
#[ApiResource(
    collectionOperations: [
        'get',
        'post' => ['security' => 'is_granted("ROLE_USER")'],
    ],
    itemOperations: [
        'get' => ['security' => 'is_granted("ROLE_USER") and object.getReader() == user'],
        'delete',
    ],
)]
#[ApiFilter(SearchFilter::class, properties: [
    'reader' => 'exact',
    'book' => 'exact',
])]
class BookRead
{
    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator")
     */
    #[Groups(['get:book'])]
    private Uuid $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="bookReads")
     * @ORM\JoinColumn(nullable=false)
     */
    private User $reader;

    /**
     * @ORM\ManyToOne(targetEntity=Book::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private Book $book;

    /**
     * @ORM\Column(type="integer", options={"default"=1})
     */
    #[Groups(['get:book'])]
    private int $count;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getReader(): User
    {
        return $this->reader;
    }

    public function setReader(User $reader): self
    {
        $this->reader = $reader;

        return $this;
    }

    public function getBook(): Book
    {
        return $this->book;
    }

    public function setBook(Book $book): self
    {
        $this->book = $book;

        return $this;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }
}
