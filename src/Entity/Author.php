<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Abstracts\Person;
use App\Repository\AuthorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=AuthorRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'post',
    ],
    itemOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'proxy_get' => [
            'method' => 'GET',
            'security' => 'is_granted("PUBLIC_ACCESS")',
            'route_name' => 'proxy_author_get',
        ],
        'put',
        'delete',
        'patch',
    ],
    attributes: ['security' => 'is_granted("ROLE_ADMIN")'],
    normalizationContext: ['groups' => ['get:author']],
)]
#[ApiFilter(SearchFilter::class, properties: [
    'name' => 'ipartial',
    'books.book' => 'exact',
])]
class Author extends Person
{
    /**
     * @ORM\OneToMany(targetEntity=BookAuthorRelation::class, mappedBy="author", orphanRemoval=true)
     */
    #[Groups(['get:author'])]
    private Collection $books;

    /**
     * @ORM\OneToOne(targetEntity=Avatar::class, inversedBy="author", cascade={"persist", "remove"})
     */
    #[Groups(['get:author', 'get:book'])]
    protected ?Avatar $image;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    #[Groups(['get:author'])]
    private ?\DateTimeInterface $birthDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    #[Groups(['get:author'])]
    private ?\DateTimeInterface $deathDate;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="authors")
     */
    #[Groups(['get:author'])]
    private ?Country $country;

    /**
     * @ORM\ManyToMany(targetEntity=Character::class, mappedBy="creators")
     */
    #[Groups(['get:author'])]
    private Collection $characters;

    public function __construct()
    {
        $this->books = new ArrayCollection();
        $this->characters = new ArrayCollection();
    }

    /**
     * @return Collection|BookAuthorRelation[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(BookAuthorRelation $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->setAuthor($this);
        }

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(?\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getDeathDate(): ?\DateTimeInterface
    {
        return $this->deathDate;
    }

    public function setDeathDate(?\DateTimeInterface $deathDate): self
    {
        $this->deathDate = $deathDate;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection|Character[]
     */
    public function getCharacters(): Collection
    {
        return $this->characters;
    }

    public function addCharacter(Character $character): self
    {
        if (!$this->characters->contains($character)) {
            $this->characters[] = $character;
            $character->addCreator($this);
        }

        return $this;
    }

    public function removeCharacter(Character $character): self
    {
        if ($this->characters->removeElement($character)) {
            $character->removeCreator($this);
        }

        return $this;
    }
}
