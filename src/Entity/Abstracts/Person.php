<?php

namespace App\Entity\Abstracts;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Avatar;
use App\Entity\Interfaces\EntityWithImageInterface;
use App\Entity\Traits\DateActionTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

abstract class Person implements EntityWithImageInterface
{
    use DateActionTrait;

    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator")
     */
    #[Groups(['get:book', 'get:author', 'get:character'])]
    protected Uuid $id;

    /**
     * @ORM\Column
     * @ApiFilter(SearchFilter::class, strategy="ipartial")
     */
    #[Groups(['get:book', 'get:author', 'get:character'])]
    protected string $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    #[Groups(['get:author', 'get:character'])]
    protected ?string $presentation;

    /**
     * @ORM\OneToOne(targetEntity=Avatar::class, inversedBy="author", cascade={"persist", "remove"})
     */
    #[Groups(['get:author', 'get:character'])]
    protected ?Avatar $image;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPresentation(): ?string
    {
        return $this->presentation;
    }

    public function setPresentation(?string $presentation): self
    {
        $this->presentation = $presentation;

        return $this;
    }

    public function getImage(): ?Avatar
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        if (!$image instanceof Avatar) {
            throw new InvalidTypeException('Image must be an instance of '.Avatar::class);
        }
        $this->image = $image;

        return $this;
    }
}
