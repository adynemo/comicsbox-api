<?php

namespace App\Entity\Abstracts;

use ApiPlatform\Core\Annotation\ApiProperty;
use App\Entity\Interfaces\EntityWithImageInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 */
abstract class Image
{
    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator")
     */
    #[Groups(['image_read'])]
    protected Uuid $id;

    /**
     * @ApiProperty(iri="http://localhost:8080/url")
     */
    #[Groups(['image_read', 'get:book', 'get:author', 'get:publisher', 'get:character', 'get:serie'])]
    protected ?string $url = null;

    /**
     * @Assert\NotNull(groups={"image_create"})
     * @Vich\UploadableField(mapping="image", fileNameProperty="filePath")
     */
    protected File $file;

    /**
     * @ORM\Column
     */
    protected string $filePath;

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getFile(): File
    {
        return $this->file;
    }

    public function setFile(File $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function setFilePath(string $filePath): self
    {
        $this->filePath = $filePath;

        return $this;
    }

    protected function checkRelation(?EntityWithImageInterface $object, string $property): void
    {
        // unset the owning side of the relation if necessary
        if (null === $object && null !== $this->$property) {
            $this->$property->setImage(null);
        }

        // set the owning side of the relation if necessary
        if (null !== $object && $object->getImage() !== $this) {
            $object->setImage($this);
        }
    }
}
