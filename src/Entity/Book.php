<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Abstracts\Image;
use App\Entity\Interfaces\EntityWithImageInterface;
use App\Entity\Traits\DateActionTrait;
use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'post',
    ],
    itemOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'proxy_get' => [
            'method' => 'GET',
            'security' => 'is_granted("PUBLIC_ACCESS")',
            'route_name' => 'proxy_book_get',
        ],
        'put',
        'delete',
        'patch',
    ],
    attributes: ['security' => 'is_granted("ROLE_ADMIN")'],
    denormalizationContext: [
        'groups' => ['post:book'],
        'enable_max_depth' => true,
    ],
    normalizationContext: ['groups' => ['get:book']],
)]
#[ApiFilter(SearchFilter::class, properties: [
    'title' => 'ipartial',
    'authors' => 'ipartial',
    'serie.id' => 'exact',
    'characters' => 'exact',
])]
#[ApiFilter(OrderFilter::class, properties: ['releaseDate', 'createdAt'], arguments: ['orderParameterName' => 'order'])]
class Book implements EntityWithImageInterface
{
    use DateActionTrait;

    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator")
     */
    #[Groups(['get:book', 'post:book'])]
    private Uuid $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    #[Groups(['get:book', 'post:book'])]
    private string $slug;

    /**
     * @ORM\Column
     */
    #[Groups(['get:book', 'post:book'])]
    private string $title;

    /**
     * @ORM\Column(type="text", options={"default": ""})
     */
    #[Groups(['get:book', 'post:book'])]
    private string $description = '';

    /**
     * @ORM\OneToOne(targetEntity=Cover::class, inversedBy="book", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    #[Groups(['get:book', 'post:book'])]
    private ?Cover $image = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    #[Groups(['get:book', 'post:book'])]
    private ?int $number;

    /**
     * @ORM\OneToMany(targetEntity=BookAuthorRelation::class, mappedBy="book", orphanRemoval=true, cascade={"persist"})
     */
    #[Groups(['get:book', 'post:book'])]
    #[MaxDepth(2)]
    private Collection $authors;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    #[Groups(['get:book', 'post:book'])]
    private ?int $pageCount;

    /**
     * @ORM\Column(type="date")
     */
    #[Groups(['get:book', 'post:book'])]
    private \DateTimeInterface $releaseDate;

    /**
     * @ORM\ManyToMany(targetEntity=Character::class, mappedBy="books", cascade={"persist"})
     */
    #[Groups(['get:book', 'post:book'])]
    #[MaxDepth(2)]
    private Collection $characters;

    /**
     * @ORM\Column(type="string", length=17, nullable=true)
     */
    #[Groups(['get:book', 'post:book'])]
    private ?string $isbn;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    #[Groups(['get:book', 'post:book'])]
    private ?string $sku;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    #[Groups(['get:book', 'post:book'])]
    private ?float $price;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class)
     * @ORM\JoinColumn(nullable=false)
     */
    #[Groups(['get:book', 'post:book'])]
    private Country $country;

    /**
     * @ORM\ManyToOne(targetEntity=Format::class, cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    #[Groups(['get:book', 'post:book'])]
    private Format $format;

    /**
     * @ORM\ManyToOne(targetEntity=Serie::class, inversedBy="books")
     */
    #[Groups(['get:book', 'post:book'])]
    #[MaxDepth(2)]
    private ?Serie $serie;

    /**
     * @ORM\ManyToMany(targetEntity=Book::class, inversedBy="repositories")
     * @ORM\JoinTable(name="issue_repositories")
     */
    #[Groups(['get:book', 'post:book'])]
    #[ApiProperty(readableLink: true, writableLink: false)]
    #[MaxDepth(1)]
    private Collection $issues;

    /**
     * @ORM\ManyToMany(targetEntity=Book::class, mappedBy="issues")
     */
    #[Groups(['get:book', 'post:book'])]
    #[ApiProperty(readableLink: true, writableLink: false)]
    #[MaxDepth(1)]
    private Collection $repositories;

    /**
     * @ORM\ManyToOne(targetEntity=Book::class, inversedBy="variants")
     */
    #[Groups(['get:book', 'post:book'])]
    #[ApiProperty(readableLink: true, writableLink: false)]
    #[MaxDepth(1)]
    private ?Book $originalBook;

    /**
     * @ORM\OneToMany(targetEntity=Book::class, mappedBy="originalBook")
     */
    #[Groups(['get:book', 'post:book'])]
    #[ApiProperty(readableLink: true, writableLink: false)]
    #[MaxDepth(1)]
    private Collection $variants;

    /**
     * @ORM\ManyToOne(targetEntity=PublisherCollection::class, inversedBy="books")
     */
    #[Groups(['get:book', 'post:book'])]
    #[MaxDepth(2)]
    private ?PublisherCollection $collection;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
        $this->characters = new ArrayCollection();
        $this->issues = new ArrayCollection();
        $this->repositories = new ArrayCollection();
        $this->variants = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getImage(): ?Cover
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        if (!$image instanceof Cover) {
            throw new InvalidTypeException('Image must be an instance of '.Cover::class);
        }
        $this->image = $image;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return Collection|BookAuthorRelation[]
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(BookAuthorRelation $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors[] = $author;
            $author->setBook($this);
        }

        return $this;
    }

    public function getPageCount(): ?int
    {
        return $this->pageCount;
    }

    public function setPageCount(?int $pageCount): self
    {
        $this->pageCount = $pageCount;

        return $this;
    }

    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(\DateTimeInterface $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    /**
     * @return Collection|Character[]
     */
    public function getCharacters(): Collection
    {
        return $this->characters;
    }

    public function addCharacter(Character $character): self
    {
        if (!$this->characters->contains($character)) {
            $this->characters[] = $character;
            $character->addBook($this);
        }

        return $this;
    }

    public function removeCharacter(Character $character): self
    {
        if ($this->characters->removeElement($character)) {
            $character->removeBook($this);
        }

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(?string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(?string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCountry(): Country
    {
        return $this->country;
    }

    public function setCountry(Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getFormat(): Format
    {
        return $this->format;
    }

    public function setFormat(Format $format): self
    {
        $this->format = $format;

        return $this;
    }

    public function getSerie(): ?Serie
    {
        return $this->serie;
    }

    public function setSerie(?Serie $serie): self
    {
        $this->serie = $serie;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getIssues(): Collection
    {
        return $this->issues;
    }

    public function addIssue(Book $issue): self
    {
        if (!$this->issues->contains($issue)) {
            $this->issues[] = $issue;
        }

        return $this;
    }

    public function removeIssue(Book $issue): self
    {
        $this->issues->removeElement($issue);

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getRepositories(): Collection
    {
        return $this->repositories;
    }

    public function addRepository(Book $repository): self
    {
        if (!$this->repositories->contains($repository)) {
            $this->repositories[] = $repository;
            $repository->addIssue($this);
        }

        return $this;
    }

    public function removeRepository(Book $repository): self
    {
        if ($this->repositories->removeElement($repository)) {
            $repository->removeIssue($this);
        }

        return $this;
    }

    public function getOriginalBook(): ?Book
    {
        return $this->originalBook;
    }

    public function setOriginalBook(?Book $originalBook): self
    {
        $this->originalBook = $originalBook;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getVariants(): Collection
    {
        return $this->variants;
    }

    public function addVariant(Book $variant): self
    {
        if (!$this->variants->contains($variant)) {
            $this->variants[] = $variant;
            $variant->setOriginalBook($this);
        }

        return $this;
    }

    public function removeVariant(Book $variant): self
    {
        if ($this->variants->removeElement($variant)) {
            // set the owning side to null (unless already changed)
            if ($variant->getOriginalBook() === $this) {
                $variant->setOriginalBook(null);
            }
        }

        return $this;
    }

    public function getCollection(): ?PublisherCollection
    {
        return $this->collection;
    }

    public function setCollection(?PublisherCollection $collection): self
    {
        $this->collection = $collection;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}
