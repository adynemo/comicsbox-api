<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Entity\Abstracts\Person;
use App\Repository\CharacterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=CharacterRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'post',
    ],
    itemOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'proxy_get' => [
            'method' => 'GET',
            'security' => 'is_granted("PUBLIC_ACCESS")',
            'route_name' => 'proxy_character_get',
        ],
        'put',
        'delete',
        'patch',
    ],
    attributes: ['security' => 'is_granted("ROLE_ADMIN")'],
    normalizationContext: ['groups' => ['get:character']],
)]
#[ApiFilter(SearchFilter::class, properties: [
    'books' => 'exact',
    'name' => 'ipartial',
])]
class Character extends Person
{
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    #[Groups(['get:character'])]
    private ?int $creationYear;

    /**
     * @ORM\ManyToOne(targetEntity=Book::class)
     */
    #[Groups(['get:character'])]
    private ?Book $firstAppearance;

    /**
     * @ORM\ManyToMany(targetEntity=Book::class, inversedBy="characters")
     */
    #[Groups(['get:character'])]
    private Collection $books;

    /**
     * @ORM\OneToOne(targetEntity=Avatar::class, inversedBy="character", cascade={"persist", "remove"})
     */
    #[Groups(['get:character', 'get:book'])]
    protected ?Avatar $image;

    /**
     * todo ManyToMany?
     *
     * @ORM\ManyToOne(targetEntity=Publisher::class, inversedBy="characters")
     */
    #[Groups(['get:character'])]
    private ?Publisher $publisher;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    #[Groups(['get:character'])]
    private ?string $history;

    /**
     * @ORM\ManyToOne(targetEntity=Character::class, inversedBy="aliases")
     */
    #[Groups(['get:character'])]
    private ?Character $mainCharacter;

    /**
     * @ORM\OneToMany(targetEntity=Character::class, mappedBy="mainCharacter")
     */
    #[Groups(['get:character'])]
    private Collection $aliases;

    /**
     * @ORM\ManyToMany(targetEntity=Author::class, inversedBy="characters")
     */
    #[Groups(['get:character'])]
    private Collection $creators;

    public function __construct()
    {
        $this->books = new ArrayCollection();
        $this->aliases = new ArrayCollection();
        $this->creators = new ArrayCollection();
    }

    public function getCreationYear(): ?int
    {
        return $this->creationYear;
    }

    public function setCreationYear(?int $creationYear): self
    {
        $this->creationYear = $creationYear;

        return $this;
    }

    public function getFirstAppearance(): ?Book
    {
        return $this->firstAppearance;
    }

    public function setFirstAppearance(?Book $firstAppearance): self
    {
        $this->firstAppearance = $firstAppearance;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        $this->books->removeElement($book);

        return $this;
    }

    public function getPublisher(): ?Publisher
    {
        return $this->publisher;
    }

    public function setPublisher(?Publisher $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }

    public function getHistory(): ?string
    {
        return $this->history;
    }

    public function setHistory(?string $history): self
    {
        $this->history = $history;

        return $this;
    }

    public function getMainCharacter(): ?Character
    {
        return $this->mainCharacter;
    }

    public function setMainCharacter(?Character $mainCharacter): self
    {
        $this->mainCharacter = $mainCharacter;

        return $this;
    }

    /**
     * @return Collection|Character[]
     */
    public function getAliases(): Collection
    {
        return $this->aliases;
    }

    public function addAlias(Character $alias): self
    {
        if (!$this->aliases->contains($alias)) {
            $this->aliases[] = $alias;
            $alias->setMainCharacter($this);
        }

        return $this;
    }

    public function removeAlias(Character $alias): self
    {
        if ($this->aliases->removeElement($alias)) {
            // set the owning side to null (unless already changed)
            if ($alias->getMainCharacter() === $this) {
                $alias->setMainCharacter(null);
            }
        }

        return $this;
    }

    public function isMain(): ?bool
    {
        return null === $this->mainCharacter && !$this->aliases->isEmpty();
    }

    /**
     * @return Collection|Author[]
     */
    public function getCreators(): Collection
    {
        return $this->creators;
    }

    public function addCreator(Author $creator): self
    {
        if (!$this->creators->contains($creator)) {
            $this->creators[] = $creator;
        }

        return $this;
    }

    public function removeCreator(Author $creator): self
    {
        $this->creators->removeElement($creator);

        return $this;
    }
}
