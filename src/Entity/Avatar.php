<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\CreateImageController;
use App\Entity\Abstracts\Image;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks()
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'post' => [
            'controller' => CreateImageController::class,
            'deserialize' => false,
            'validation_groups' => ['Default', 'image_create'],
            'openapi_context' => [
                'requestBody' => [
                    'content' => [
                        'multipart/form-data' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file' => [
                                        'type' => 'string',
                                        'format' => 'binary',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    itemOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'delete' => ['security' => 'is_granted("ROLE_ADMIN") or object.getUser() == user'],
    ],
    attributes: ['security' => 'is_granted("ROLE_USER")'],
    normalizationContext: ['groups' => ['image_read']],
)]
class Avatar extends Image
{
    /**
     * @Assert\NotNull(groups={"image_create"})
     * @Vich\UploadableField(mapping="avatar", fileNameProperty="filePath")
     */
    protected File $file;

    /**
     * @ORM\OneToOne(targetEntity=Author::class, mappedBy="image", cascade={"persist"})
     */
    #[Groups(['image_read'])]
    private ?Author $author;

    /**
     * @ORM\OneToOne(targetEntity=Character::class, mappedBy="image", cascade={"persist"})
     */
    #[Groups(['image_read'])]
    private ?Character $character;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="image", cascade={"persist"})
     */
    #[Groups(['image_read'])]
    private ?User $user;

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {
        $this->checkRelation($author, 'author');

        $this->author = $author;

        return $this;
    }

    public function getCharacter(): ?Character
    {
        return $this->character;
    }

    public function setCharacter(?Character $character): self
    {
        $this->checkRelation($character, 'character');

        $this->character = $character;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->checkRelation($user, 'user');

        $this->user = $user;

        return $this;
    }

    /**
     * @ORM\PreRemove
     */
    public function updateRelations(): void
    {
        $this->author?->setImage(null);
        $this->character?->setImage(null);
        $this->user?->setImage(null);
    }
}
