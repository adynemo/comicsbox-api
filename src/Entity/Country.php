<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\CountryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

/**
 * @ORM\Entity(repositoryClass=CountryRepository::class)
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'post',
    ],
    itemOperations: [
        'get' => ['security' => 'is_granted("PUBLIC_ACCESS")'],
        'put',
        'delete',
        'patch',
    ],
    attributes: ['security' => 'is_granted("ROLE_ADMIN")'],
    denormalizationContext: ['groups' => ['post:country']],
    normalizationContext: ['groups' => ['get:country']],
)]
class Country
{
    /**
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator")
     */
    #[Groups(['get:country', 'get:book'])]
    protected Uuid $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(['get:country', 'get:book', 'post:country'])]
    private string $name;

    /**
     * @ORM\Column(type="string", length=2)
     */
    #[Groups(['get:country', 'get:book', 'post:country'])]
    private string $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    #[Groups(['get:country', 'get:book', 'post:country'])]
    private string $currency;

    /**
     * @ORM\OneToMany(targetEntity=Publisher::class, mappedBy="country")
     */
    #[Groups(['get:country'])]
    private Collection $publishers;

    /**
     * @ORM\OneToMany(targetEntity=Author::class, mappedBy="country")
     */
    #[Groups(['get:country'])]
    private Collection $authors;

    public function __construct()
    {
        $this->publishers = new ArrayCollection();
        $this->authors = new ArrayCollection();
    }

    public function getId(): Uuid
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return Collection|Publisher[]
     */
    public function getPublishers(): Collection
    {
        return $this->publishers;
    }

    public function addPublisher(Publisher $publisher): self
    {
        if (!$this->publishers->contains($publisher)) {
            $this->publishers[] = $publisher;
            $publisher->setCountry($this);
        }

        return $this;
    }

    public function removePublisher(Publisher $publisher): self
    {
        if ($this->publishers->removeElement($publisher)) {
            // set the owning side to null (unless already changed)
            if ($publisher->getCountry() === $this) {
                $publisher->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Author[]
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors[] = $author;
            $author->setCountry($this);
        }

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        if ($this->authors->removeElement($author)) {
            // set the owning side to null (unless already changed)
            if ($author->getCountry() === $this) {
                $author->setCountry(null);
            }
        }

        return $this;
    }
}
