<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

trait DateActionTrait
{
    /**
     * @ORM\Column(type="date")
     */
    #[Groups(['get:book', 'get:author', 'get:serie'])]
    protected \DateTimeInterface $createdAt;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    #[Groups(['get:book', 'get:author', 'get:serie'])]
    protected ?\DateTimeInterface $updatedAt;

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): self
    {
        $this->createdAt = new \DateTimeImmutable();

        return $this;
    }

    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAt(): self
    {
        $this->updatedAt = new \DateTimeImmutable();

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }
}
