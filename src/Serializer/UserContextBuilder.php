<?php

namespace App\Serializer;

use ApiPlatform\Core\Serializer\SerializerContextBuilderInterface;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Security;

final class UserContextBuilder implements SerializerContextBuilderInterface
{
    public function __construct(
        private SerializerContextBuilderInterface $decorated,
        private AuthorizationCheckerInterface $authorizationChecker,
        private Security $security
    ) {
    }

    public function createFromRequest(Request $request, bool $normalization, ?array $extractedAttributes = null): array
    {
        $context = $this->decorated->createFromRequest($request, $normalization, $extractedAttributes);
        $resourceClass = $context['resource_class'] ?? null;
        /**
         * @var User $user
         */
        $user = $this->security->getUser();

        if (User::class === $resourceClass && isset($context['groups']) && true === $normalization && $user instanceof User) {
            $context['groups'] = [];
            $uri = sprintf('%s/%s', User::BASE_URI, $user->getId());

            if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                $context['groups'][] = 'admin:get';
            } elseif ($this->authorizationChecker->isGranted('ROLE_USER') && $context['request_uri'] === $uri) {
                $context['groups'][] = 'user:get:own';
            } elseif ($this->authorizationChecker->isGranted('ROLE_USER')) {
                $context['groups'][] = 'user:get:other';
            }
        }

        return $context;
    }
}
