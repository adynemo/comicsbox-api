<?php

namespace App\Transformer;

use App\DAO\Book as BookDAO;
use App\Entity\Book;
use App\Transformer\Traits\ExportFieldTrait;

class BookTransformer
{
    use ExportFieldTrait;

    public function transform(Book $book): BookDAO
    {
        $dao = new BookDAO();

        $this->export([$book, 'getId'], [$dao, 'setId']);
        $this->export([$book, 'getTitle'], [$dao, 'setTitle']);
        $this->export([$book, 'getNumber'], [$dao, 'setNumber']);
        $this->export([$book, 'getReleaseDate'], [$dao, 'setReleaseDate']);
        $this->export([$book, 'getImage'], [$dao, 'setImage']);

        return $dao;
    }
}
