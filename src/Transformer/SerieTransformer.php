<?php

namespace App\Transformer;

use App\DAO\Serie as SerieDAO;
use App\Entity\Serie;
use App\Transformer\Traits\ExportFieldTrait;

class SerieTransformer
{
    use ExportFieldTrait;

    public function transform(Serie $serie): SerieDAO
    {
        $dao = new SerieDAO();

        $this->export([$serie, 'getId'], [$dao, 'setId']);
        $this->export([$serie, 'getTitle'], [$dao, 'setTitle']);
        $this->export([$serie, 'getBeginYear'], [$dao, 'setBeginYear']);
        $this->export([$serie, 'getEndYear'], [$dao, 'setEndYear']);
        $this->export([$serie, 'getImage'], [$dao, 'setImage']);

        return $dao;
    }
}
