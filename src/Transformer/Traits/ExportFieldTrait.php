<?php

namespace App\Transformer\Traits;

trait ExportFieldTrait
{
    private function export(callable $getter, callable $setter, mixed $nullValue = null): void
    {
        if ($nullValue !== $value = (call_user_func($getter))) {
            call_user_func($setter, $value);
        }
    }
}
