<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateUserController
{
    public function __construct(private UserPasswordHasherInterface $encoder)
    {
    }

    public function __invoke(User $data): User
    {
        if (null !== $data->getPassword() && '' !== $data->getPassword()) {
            $data
                ->setPassword(
                    $this->encoder->hashPassword(
                        $data,
                        $data->getPassword()
                    )
                );
        }

        return $data;
    }
}
