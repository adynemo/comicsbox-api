<?php

namespace App\Controller\Proxy;

use App\Entity\Author;
use App\Service\PersonService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/authors')]
#[AsController]
class AuthorController extends AbstractController
{
    public function __construct(
        private PersonService $personService
    ) {
    }

    #[Route(
        path: '/{id}',
        name: 'proxy_author_get',
        defaults: [
            '_api_resource_class' => Author::class,
            '_api_item_operation_name' => 'proxy_get',
        ]
    )]
    public function getOne(Author $author): array
    {
        $extendedPerson = $this->personService->convertPersonForProxyAPI($author);

        return [[
            'author' => $extendedPerson['person'],
            'series' => $extendedPerson['series'],
        ]];
    }
}
