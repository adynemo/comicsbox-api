<?php

namespace App\Controller\Proxy;

use App\Entity\Book;
use App\Entity\BookRead;
use App\Repository\BookReadRepository;
use App\Repository\BookRepository;
use App\Service\ImageService;
use App\Service\StringModifier;
use Doctrine\Persistence\ManagerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/books')]
#[AsController]
class BookController extends AbstractController
{
    public function __construct(private ImageService $imageService)
    {
    }

    #[Route(
        path: '/{id}',
        name: 'proxy_book_get',
        defaults: [
            '_api_resource_class' => Book::class,
            '_api_item_operation_name' => 'proxy_get',
        ]
    )]
    public function getOne(Book $book, ManagerRegistry $registry): array
    {
        $this->imageService->setImageUrl($book);
        $this->imageService->setImageUrl($book->getOriginalBook());
        foreach ($book->getAuthors() as $author) {
            $this->imageService->setImageUrl($author->getAuthor());
        }
        $this->imageService->setImageUrlFromIterable($book->getCharacters());
        $this->imageService->setImageUrlFromIterable($book->getIssues());
        $this->imageService->setImageUrlFromIterable($book->getRepositories());
        $this->imageService->setImageUrlFromIterable($book->getVariants());

        /** @var BookRepository $repository */
        $repository = $registry->getRepository(Book::class);
        $previous = $repository->findPreviousBook($book);
        $next = $repository->findNextBook($book);

        $user = $this->getUser();
        $read = null;
        if (null !== $user) {
            /** @var BookReadRepository $repository */
            $repository = $registry->getRepository(BookRead::class);
            $read = $repository->findOneBy(['reader' => $user, 'book' => $book]);
        }

        return [[
            'previous' => $previous,
            'current' => $book,
            'next' => $next,
            'read' => $read,
        ]];
    }

    #[Route('/search/isbn/{isbn}')]
    #[IsGranted('ROLE_USER')]
    public function searchByISBN(string $isbn, BookRepository $repository, StringModifier $stringModifier): JsonResponse
    {
        // todo: check if is ISBN
        $isbn = $stringModifier->clearISBN($isbn);
        $books = $repository->findBy(['isbn' => $isbn]);

        return $this->json($books);
    }
}
