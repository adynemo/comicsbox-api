<?php

namespace App\Controller\Proxy;

use App\Entity\Serie;
use App\Repository\BookRepository;
use App\Service\ImageService;
use App\Transformer\BookTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/series')]
#[AsController]
class SerieController extends AbstractController
{
    public function __construct(
        private ImageService $imageService,
        private BookTransformer $bookTransformer
    ) {
    }

    #[Route(
        path: '/{id}',
        name: 'proxy_serie_get',
        defaults: [
            '_api_resource_class' => Serie::class,
            '_api_item_operation_name' => 'proxy_get',
        ]
    )]
    public function getOne(Serie $serie, BookRepository $repository): array
    {
        $this->imageService->setImageUrl($serie);
        $results = $repository->findBooksBySerie($serie);

        $books = new \ArrayIterator();
        foreach ($results as $result) {
            $this->imageService->setImageUrl($result);
            $books->append($this->bookTransformer->transform($result));
        }

        return [[
            'serie' => $serie,
            'books' => $books,
        ]];
    }
}
