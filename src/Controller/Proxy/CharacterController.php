<?php

namespace App\Controller\Proxy;

use App\Entity\Character;
use App\Service\ImageService;
use App\Service\PersonService;
use App\Transformer\BookTransformer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/characters')]
#[AsController]
class CharacterController extends AbstractController
{
    public function __construct(
        private PersonService $personService,
        private ImageService $imageService,
        private BookTransformer $bookTransformer
    ) {
    }

    #[Route(
        path: '/{id}',
        name: 'proxy_character_get',
        defaults: [
            '_api_resource_class' => Character::class,
            '_api_item_operation_name' => 'proxy_get',
        ]
    )]
    public function getOne(Character $character): array
    {
        $extendedPerson = $this->personService->convertPersonForProxyAPI($character);
        $firstAppearance = [];
        $book = $character->getFirstAppearance();

        if (null !== $book) {
            $this->imageService->setImageUrl($book);
            $firstAppearance = $this->bookTransformer->transform($book);
        }

        return [[
            'character' => $extendedPerson['person'],
            'firstAppearance' => $firstAppearance,
            'series' => $extendedPerson['series'],
        ]];
    }
}
