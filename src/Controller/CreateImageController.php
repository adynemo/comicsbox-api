<?php

namespace App\Controller;

use App\Entity\Abstracts\Image;
use App\Entity\Avatar;
use App\Entity\Cover;
use App\Entity\Logo;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

final class CreateImageController
{
    private const ALLOWED_RESOURCES = [
        Avatar::class,
        Cover::class,
        Logo::class,
    ];

    public function __invoke(Request $request): Image
    {
        $resource = $request->attributes->get('_api_resource_class');
        if (in_array($resource, self::ALLOWED_RESOURCES)) {
            /** @var Image $image */
            $image = new $resource();

            return $this->create($request, $image);
        }

        throw new BadRequestException('You try to create a not allowed resource');
    }

    private function create(Request $request, Image $image): Image
    {
        /**
         * @var ?UploadedFile $uploadedFile
         */
        $uploadedFile = $request->files->get('file');
        if (null === $uploadedFile) {
            throw new BadRequestHttpException('"file" is required');
        }

        $image->setFile($uploadedFile);

        return $image;
    }
}
