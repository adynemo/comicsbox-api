<?php

namespace App\Controller;

use App\Service\StatsCalculator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/stats')]
class StatsController extends AbstractController
{
    #[Route('/global')]
    #[IsGranted('ROLE_USER')]
    public function globalStats(StatsCalculator $calculator): JsonResponse
    {
        return new JsonResponse($calculator->getAllStats());
    }
}
