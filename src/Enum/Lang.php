<?php

namespace App\Enum;

class Lang
{
    public const EN = 'en';
    public const FR = 'fr';
}
