<?php

namespace App\EventListener;

use App\Entity\Abstracts\Image;
use App\Entity\Avatar;
use App\Entity\Logo;
use App\Service\ImageOptimizer;
use Vich\UploaderBundle\Event\Event;

class ImageUploadListener
{
    private const SIZE = [
        Avatar::class => [
            'width' => 150,
            'height' => 150,
        ],
        Logo::class => [
            'width' => 150,
            'height' => 150,
        ],
    ];

    public function __construct(private ImageOptimizer $resizer)
    {
    }

    public function onVichUploaderPostUpload(Event $event): void
    {
        $object = $event->getObject();
        $class = \get_class($object);

        if ($object instanceof Image && isset(self::SIZE[$class])) {
            $this->resizer->resize($object->getFile()->getPathname(), self::SIZE[$class]['width'], self::SIZE[$class]['height']);
        }
    }
}
