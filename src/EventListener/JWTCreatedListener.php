<?php

namespace App\EventListener;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\Security\Core\Security;
use Vich\UploaderBundle\Storage\StorageInterface;

class JWTCreatedListener
{
    public function __construct(private Security $security, private StorageInterface $storage)
    {
    }

    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        /**
         * @var User $user
         */
        $user = $this->security->getUser();
        $avatarUrl = null !== $user->getImage() ? $this->storage->resolveUri($user->getImage(), 'file') : null;

        $expiration = new \DateTime('+7 day');
        $expiration->setTime(2, 0);

        $addToPayload = [
            'id' => $user->getId()->toRfc4122(),
            'email' => $user->getEmail(),
            'avatar' => $avatarUrl,
            'exp' => $expiration->getTimestamp(),
        ];

        $payload = array_merge($event->getData(), $addToPayload);

        $event->setData($payload);
    }
}
