<?php

namespace App\DAO;

use App\Entity\Cover;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

class Book
{
    #[Groups(['get:character', 'get:author', 'get:serie', 'get:book'])]
    private ?Uuid $id;
    #[Groups(['get:character', 'get:author', 'get:serie', 'get:book'])]
    private ?string $title;
    #[Groups(['get:character', 'get:author', 'get:serie', 'get:book'])]
    private ?int $number;
    #[Groups(['get:character', 'get:author', 'get:serie', 'get:book'])]
    private ?\DateTime $releaseDate;
    #[Groups(['get:character', 'get:author', 'get:serie', 'get:book'])]
    private ?Cover $image = null;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): Book
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): Book
    {
        $this->title = $title;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): Book
    {
        $this->number = $number;

        return $this;
    }

    public function getReleaseDate(): ?\DateTime
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(?\DateTime $releaseDate): Book
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function getImage(): ?Cover
    {
        return $this->image;
    }

    public function setImage(?Cover $image): Book
    {
        $this->image = $image;

        return $this;
    }
}
