<?php

namespace App\DAO;

use App\Entity\Cover;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Uid\Uuid;

class Serie
{
    #[Groups(['get:character', 'get:author', 'get:serie', 'get:book'])]
    private ?Uuid $id;
    #[Groups(['get:character', 'get:author', 'get:serie', 'get:book'])]
    private ?string $title;
    #[Groups(['get:character', 'get:author', 'get:serie', 'get:book'])]
    private ?Cover $image = null;
    #[Groups(['get:character', 'get:author', 'get:serie', 'get:book'])]
    private ?int $beginYear;
    #[Groups(['get:character', 'get:author', 'get:serie', 'get:book'])]
    private ?int $endYear;

    public function getId(): ?Uuid
    {
        return $this->id;
    }

    public function setId(?Uuid $id): Serie
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): Serie
    {
        $this->title = $title;

        return $this;
    }

    public function getImage(): ?Cover
    {
        return $this->image;
    }

    public function setImage(?Cover $image): Serie
    {
        $this->image = $image;

        return $this;
    }

    public function getBeginYear(): ?int
    {
        return $this->beginYear;
    }

    public function setBeginYear(?int $beginYear): Serie
    {
        $this->beginYear = $beginYear;

        return $this;
    }

    public function getEndYear(): ?int
    {
        return $this->endYear;
    }

    public function setEndYear(?int $endYear): Serie
    {
        $this->endYear = $endYear;

        return $this;
    }
}
